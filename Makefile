CC = gcc
CFLAGS = -g -w
LIBS = -L./ffmpeg/lib -lavcodec -lavformat -lavutil -lmingw32
INCLUDE = -I./ffmpeg/include
BUILD_DIR = .\exe
RM = del
SRC = $(wildcard *.c)
EXEFILE = encoder.exe

# 测试复用
muxer: 
	gcc -o $(BUILD_DIR)/muxer muxer.c $(CFLAGS) $(INCLUDE) $(LIBS)

remuxer:
	gcc -o $(BUILD_DIR)/remuxer remuxer.c $(CFLAGS) $(INCLUDE) $(LIBS)

encoder:
	gcc -o $(BUILD_DIR)/encode encode.c $(CFLAGS) $(INCLUDE) $(LIBS)

clean:
	$(RM) $(BUILD_DIR)\$(EXEFILE)

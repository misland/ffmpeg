/*
  author:Loki Zhao
  date:2021.3.24
  description:根据ffmpeg官方示例，将裸流数据（没封装前的码流）解码，将解码后的yuv数据写入到输出文件out.yuv，可以使用YUVPlayer播放
*/

#include <stdio.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/frame.h>
#include <libavutil/imgutils.h>

#define INBUF_SIZE 4096

FILE *out;

static void save_file_y(unsigned char *buf, int wrap, int xsize, int ysize)
{
  for (int i = 0; i < ysize; i++)
  {
    fwrite(buf + i * wrap, 1, xsize, out);
  }
}

static void save_file_yuv(AVFrame *frame)
{
  int line_size = frame->width * frame->height;
  // 一帧的yuv数据，分别要连续写，即先把y分量写完，再写u分量，然后是v分量
  fwrite(frame->data[0], 1, line_size, out);
  fwrite(frame->data[1], 1, line_size / 4, out);
  fwrite(frame->data[2], 1, line_size / 4, out);
}

static int decode(AVCodecContext *codec_ctx, AVFrame *frame, AVPacket *pkt, const char *filename)
{
  char buf[1024];
  int ret;

  ret = avcodec_send_packet(codec_ctx, pkt);
  if (ret < 0)
  {
    printf("ERROR:sending packet failed\n");
    return -1;
  }

  while (ret >= 0)
  {
    ret = avcodec_receive_frame(codec_ctx, frame);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
    {
      return 0;
    }
    else if (ret < 0)
    {
      printf("ERROR:decoding packet failed\n");
      return -1;
    }
    // printf("INFO:saving frame %3d\n", codec_ctx->frame_number);
    save_file_y(frame->data[0], frame->linesize[0], codec_ctx->width, codec_ctx->height);
    // save_file_yuv(frame);
    // printf("INFO:linesize[0]:%d,linesize[1]:%d,linesize[2]:%d\n",
    //        frame->linesize[0], frame->linesize[1], frame->linesize[2]);
  }
  return 0;
}

int main(int argc, char *argv[])
{
  if (argc < 2)
  {
    printf("INFO:please specify the file name\n");
    return -1;
  }

  if ((out = fopen("out.yuv", "w")) == NULL)
  {
    printf("ERROR:open output file failed\n");
    return -1;
  }

  AVFormatContext *fmt_ctx = NULL;
  AVCodecContext *codec_ctx = NULL;
  AVCodec *video_codec = NULL;
  AVCodec *audio_codec = NULL;
  AVCodecParserContext *parser;
  AVFrame *frame;
  uint8_t inbuf[INBUF_SIZE + AV_INPUT_BUFFER_PADDING_SIZE];
  uint8_t *data;
  size_t data_size;
  int ret;
  int video_index = -1;
  int audio_index = -1;
  AVPacket *pkt;
  FILE *fp;

  pkt = av_packet_alloc();
  if (!pkt)
  {
    printf("ERROR: allocate memory for packet error\n");
    return -1;
  }

  memset(inbuf + INBUF_SIZE, 0, AV_INPUT_BUFFER_PADDING_SIZE);
  ret = avformat_open_input(&fmt_ctx, argv[1], NULL, NULL);
  if (ret != 0)
  {
    printf("ERROR:open input failed,error no:%d\n", ret);
    goto exit;
    // return -1;
  }

  ret = avformat_find_stream_info(fmt_ctx, NULL);
  if (ret < 0)
  {
    printf("ERROR:find stream info failed,error no:%d\n", ret);
    goto exit;
  }
  if (fmt_ctx->nb_streams > 0)
  {
    for (int i = 0; i < fmt_ctx->nb_streams; i++)
    {
      printf("INFO:stream[%d]:duration:%d,codec_type:%d,codec_id:%d\n",
             i, fmt_ctx->streams[i]->duration,
             fmt_ctx->streams[i]->codecpar->codec_type,
             fmt_ctx->streams[i]->codecpar->codec_id);
      if (fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
      {
        audio_index = i;
        audio_codec = avcodec_find_decoder(fmt_ctx->streams[i]->codecpar->codec_id);
        printf("INFO:codec name:%s,long_name:%s\n", audio_codec->name, audio_codec->long_name);
        if (audio_codec == NULL)
        {
          printf("ERROR:find audio decoder failed\n");
          return -1;
        }
      }
      else if (fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
      {
        video_index = i;
        video_codec = avcodec_find_decoder(fmt_ctx->streams[i]->codecpar->codec_id);
        if (video_codec == NULL)
        {
          printf("ERROR:find video decoder failed\n");
          return -1;
        }
        printf("INFO:codec name:%s,long_name:%s\n", video_codec->name, video_codec->long_name);
      }
      else
      {
        // do something else
      }
    }
  }
  else
  {
    printf("INFO:does not find streams in given file\n");
    goto exit;
  }

  parser = av_parser_init(video_codec->id);
  if (!parser)
  {
    printf("ERROR:init parser failed\n");
    goto exit;
  }

  codec_ctx = avcodec_alloc_context3(video_codec);
  if (!codec_ctx)
  {
    printf("ERROR:allocate video codec failed\n");
    goto exit;
  }

  if (avcodec_open2(codec_ctx, video_codec, NULL) < 0)
  {
    printf("ERROR:open video codec failed\n");
    goto exit;
  }

  fp = fopen(argv[1], "rb");
  if (!fp)
  {
    printf("ERROR:open file failed\n");
    goto exit;
  }

  frame = av_frame_alloc();
  if (!frame)
  {
    printf("ERROR:allocate frame failed\n");
    goto exit;
  }

  while (!feof(fp))
  {
    data_size = fread(inbuf, 1, INBUF_SIZE, fp);
    if (!data_size)
    {
      break;
    }
    data = inbuf;
    while (data_size > 0)
    {
      ret = av_parser_parse2(parser, codec_ctx, &pkt->data, &pkt->size, data, data_size, AV_NOPTS_VALUE, AV_NOPTS_VALUE, 0);
      if (ret < 0)
      {
        printf("ERROR:parse failed\n");
        goto exit;
      }
      printf("INFO:ret:%d,packet size:%d\n", ret, pkt->size);
      data += ret;
      data_size -= ret;
      if (pkt->size > 0)
      {
        decode(codec_ctx, frame, pkt, "out.yuv");
      }
    }
  }
exit:
  decode(codec_ctx, frame, NULL, "out.yuv");
  fclose(fp);
  fclose(out);
  av_parser_close(parser);
  av_frame_free(&frame);
  av_packet_unref(pkt);
  avcodec_free_context(&codec_ctx);
  return 0;
}

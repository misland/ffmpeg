/*
  视频播放器 version 1，只有最简单的视频播放功能
  
*/

#include <stdio.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_rect.h>

int main(int argc, char *argv[])
{
    AVFormatContext *p_fmt_ctx = NULL;
    AVCodecContext *p_codec_ctx = NULL;
    AVCodecParameters *p_codec_par = NULL;
    AVCodec *p_codec = NULL;
    AVFrame *p_frm_raw = NULL;
    AVFrame *p_frm_yuv = NULL;
    AVPacket *p_packet = NULL;
    struct SwsContext *sws_ctx = NULL;
    int buf_size;
    uint8_t *buffer = NULL;
    int i;
    int v_idx;
    int ret;
    SDL_Window *screen;
    SDL_Renderer *sdl_renderer;
    SDL_Texture *sdl_texture;
    SDL_Rect sdl_rect;

    if (argc < 2)
    {
        printf("please specify the video to play\n");
        return -1;
    }

    ret = avformat_open_input(&p_fmt_ctx, argv[1], NULL, NULL);
    if (ret != 0)
    {
        printf("avformat_open_input failed\n");
        return -1;
    }

    ret = avformat_find_stream_info(p_fmt_ctx, NULL);
    if (ret < 0)
    {
        printf("avformat_find_stream_info failed \n");
        return -1;
    }

    // av_dump_format(p_fmt_ctx, 0, argv[1], 0);

    v_idx = -1;
    for (i = 0; i < p_fmt_ctx->nb_streams; i++)
    {
        if (p_fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
        {
            v_idx = i;
            printf("streams[%d] is video stream \n", v_idx);
            break;
        }
    }

    if (v_idx == -1)
    {
        printf("cannot find video stream,please be sure the file you selected is a video file\n");
        return -1;
    }

    // 将视频解码器参数赋值给p_codec_par
    p_codec_par = p_fmt_ctx->streams[v_idx]->codecpar;
    // 有了解码器信息，可以在ffmpeg组件中寻找对应解码器了
    p_codec = avcodec_find_decoder(p_codec_par->codec_id);
    if (p_codec == NULL)
    {
        printf("cannot find proper decoder for this file format\n");
        return -1;
    }
    // 找到解码器后，用解码器初始化结构体p_codec_ctx
    p_codec_ctx = avcodec_alloc_context3(p_codec);
    // todo
    // 暂时还不理解，已经用p_codec_par中包含的p_codec初始化了p_codec_ctx，为什么还要继续这一步？
    ret = avcodec_parameters_to_context(p_codec_ctx, p_codec_par);
    if (ret < 0)
    {
        printf("avcodec_parameters_to_context failed,error no:%d\n", ret);
        return -1;
    }

    ret = avcodec_open2(p_codec_ctx, p_codec, NULL);
    if (ret < 0)
    {
        printf("avcodec_open2 failed,error no:%d\n", ret);
        return -1;
    }

    p_frm_raw = av_frame_alloc();
    p_frm_yuv = av_frame_alloc();

    // need to import : <libavutil/imgutils.h>
    // 根据视频的分辨率计算一帧yuv格式数据占用多少字节
    buf_size = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, p_codec_ctx->width, p_codec_ctx->height, 1);

    buffer = (uint8_t *)av_malloc(buf_size);
    // 为p_frm_yuv分配空间，因为没有方法为其分配空间，所以事先手动分配
    // p_frm_raw在av_read_frame时会分配，所以这里不用手动分配
    av_image_fill_arrays(p_frm_yuv->data, p_frm_yuv->linesize, buffer,
                         AV_PIX_FMT_YUV420P, p_codec_ctx->width, p_codec_ctx->height, 1);

    //初始化SwsContext，用于后面的sws_scale
    sws_ctx = sws_getContext(p_codec_ctx->width, p_codec_ctx->height, p_codec_ctx->pix_fmt, p_codec_ctx->width, p_codec_ctx->height,
                             AV_PIX_FMT_YUV420P, SWS_BICUBIC,
                             NULL, NULL, NULL);

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
    {
        printf("SDL_Init failed,error no:%s\n", SDL_GetError());
        return -1;
    }

    screen = SDL_CreateWindow("player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                              p_codec_ctx->width, p_codec_ctx->height,
                              SDL_WINDOW_OPENGL);
    if (screen == NULL)
    {
        printf("SDL_CreateWindow failed,error no:%s\n", SDL_GetError());
        return -1;
    }

    sdl_renderer = SDL_CreateRenderer(screen, -1, 0);

    //创建SDL_Texture
    //一个SDL_Texture对应一帧YUV数据
    //在ffmpeg中的AV_PIX_FMT_YUV420P，SDL中对应的是SDL_PIXELFORMAT_IYUV
    sdl_texture = SDL_CreateTexture(sdl_renderer,
                                    SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,
                                    p_codec_ctx->width, p_codec_ctx->height);

    sdl_rect.x = 0;
    sdl_rect.y = 0;
    sdl_rect.w = p_codec_ctx->width;
    sdl_rect.h = p_codec_ctx->height;

    p_packet = (AVPacket *)av_malloc(sizeof(AVPacket));
    //开始从视频文件中读取packet
    //packet可能是视频、音频或者其他格式数据，但是解码器只能解视频和音频
    //非视频和音频格式数据并不会被丢弃，用于向解码器提供信息
    //视频：一个packet只包含一帧图像，即一个frame
    //音频：若帧长固定，一个packet可能包含整个frame，若帧长可变，一个packet只会包含一个frame
    while (av_read_frame(p_fmt_ctx, p_packet) == 0)
    {
        //只读视频
        if (p_packet->stream_index == v_idx)
        {
            //读到一个经过编码的frame，发送给解码器
            ret = avcodec_send_packet(p_codec_ctx, p_packet);
            if (ret != 0)
            {
                printf("avcodec_send_packet failed,error no:%d \n", ret);
                return -1;
            }

            //解码器接收编码后的帧，解码成原始帧
            ret = avcodec_receive_frame(p_codec_ctx, p_frm_raw);
            if (ret != 0)
            {
                printf("avcodec_receive_frame failed,error no:%d\n", ret);
                return -1;
            }

            // 将原始帧转换为yuv420格式
            sws_scale(sws_ctx,
                      (const uint8_t *const *)p_frm_raw->data, p_frm_raw->linesize,
                      0, p_codec_ctx->height,
                      p_frm_yuv->data, p_frm_yuv->linesize);
            SDL_UpdateYUVTexture(sdl_texture, &sdl_rect,
                                 p_frm_yuv->data[0],     // y
                                 p_frm_yuv->linesize[0], // y length
                                 p_frm_yuv->data[1],     // u
                                 p_frm_yuv->linesize[1],
                                 p_frm_yuv->data[2], // v
                                 p_frm_yuv->linesize[2]);

            SDL_RenderClear(sdl_renderer);

            SDL_RenderCopy(sdl_renderer, sdl_texture, NULL, &sdl_rect);

            SDL_RenderPresent(sdl_renderer);

            SDL_Delay(40);
        }

        av_packet_unref(p_packet);
    }
    printf("data read completely\n");

    SDL_Quit();
    sws_freeContext(sws_ctx);
    av_free(buffer);
    av_frame_free(&p_frm_yuv);
    av_frame_free(&p_frm_raw);
    avcodec_close(p_codec_ctx);
    avformat_close_input(&p_fmt_ctx);

    return 0;
}
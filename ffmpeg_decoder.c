
#include <stdio.h>

#include "libavcodec/avcodec.h"
#include "libavformat/avformat.h"
#include "libavutil/imgutils.h"
#include "libswscale/swscale.h"

int saveAsJPEG(AVFrame* pFrame, int width, int height, int index)
{
	char out_file[256] = { 0 };
	sprintf_s(out_file, sizeof(out_file), "%s%d.jpg", "", index);
	AVFormatContext* pFormatCtx = avformat_alloc_context();
	pFormatCtx->oformat = av_guess_format("mjpeg", NULL, NULL);
	if (avio_open(&pFormatCtx->pb, out_file, AVIO_FLAG_READ_WRITE) < 0)
	{
		printf("Couldn't open output file.");
		return -1;
	}
	AVStream* pAVStream = avformat_new_stream(pFormatCtx, 0);
	if (pAVStream == NULL)
	{
		return -1;
	}
	AVCodecContext* pCodecCtx = pAVStream->codec;
	pCodecCtx->codec_id = pFormatCtx->oformat->video_codec;
	pCodecCtx->codec_type = AVMEDIA_TYPE_VIDEO;
	pCodecCtx->pix_fmt = AV_PIX_FMT_YUVJ420P;
	pCodecCtx->width = width;
	pCodecCtx->height = height;
	pCodecCtx->time_base.num = 1;
	pCodecCtx->time_base.den = 25;
	//打印输出相关信息
	av_dump_format(pFormatCtx, 0, out_file, 1);
	//================================== 查找编码器 ==================================//
	AVCodec* pCodec = avcodec_find_encoder(pCodecCtx->codec_id);
	if (!pCodec)
	{
		printf("Codec not found.");
		return -1;
	}
	if (avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
	{
		printf("Could not open codec.");
		return -1;
	}
	//================================Write Header ===============================//
	avformat_write_header(pFormatCtx, NULL);
	int y_size = pCodecCtx->width * pCodecCtx->height;
	AVPacket pkt;
	av_new_packet(&pkt, y_size * 3);

	//
	int got_picture = 0;
	int ret = avcodec_encode_video2(pCodecCtx, &pkt, pFrame, &got_picture);
	if (ret < 0)
	{
		printf("Encode Error.\n");
		return -1;
	}
	if (got_picture == 1)
	{
		pkt.stream_index = pAVStream->index;
		ret = av_write_frame(pFormatCtx, &pkt);
	}
	av_free_packet(&pkt);
	av_write_trailer(pFormatCtx);
	if (pAVStream)
	{
		avcodec_close(pAVStream->codec);
	}
	avio_close(pFormatCtx->pb);
	avformat_free_context(pFormatCtx);
	return 0;
}

int main(int argc, char* argv[])
{
	int flag = 0;
	//初始化组件
	av_register_all();
	unsigned int version = avcodec_version();
	printf("ffmpeg version is : %#X \n", version);

	//最顶层的结构体，存储全部组件，如解码器，媒体信息如分辨率等
	AVFormatContext* formatCtx;
	//属于AVFormatContext中的streams中，存储的是媒体的编码信息，还有一些其它信息，如码率、时基等
	AVCodecContext* codecCtx;
	//属于AVCodecContext，更详细的编码信息，如编码器名字，如mp4文件是用xxx编码器编的，短名字，长名字等
	AVCodec* codec;
	//存储解码后的视频流
	AVFrame* frame, * frameYUV;
	//存储读取的未解码的视频流
	AVPacket* packet;
	//记录媒体中视频流的索引，ffmpeg获取媒体信息后，一个存储音频信息，一个存储视频信息
	int videoIndex;
	//作用是存放申请的内存空间，大小是一个像素的YUV数据
	unsigned char* buffer;
	//存放解析视频的分辨率
	int width, height;
	struct SwsContext* img_convert_ctx;

	// char* filename = "C:\\shot\\video\\test\\player\\exe\\3.mp4";
  char *filename=argv[1];
	//char *filename = "Titanic.ts";
	//全局初始化网络组件，不是必须的
	avformat_network_init();
	//申请空间的，根据结构体大小
	formatCtx = avformat_alloc_context();
	//打开指定的文件，获取文件的各种信息
	if (avformat_open_input(&formatCtx, filename, NULL, NULL) != 0)
	{
		printf("open video failed \n");
		return -1;
	}
	//打开后开始查找文件信息
	if (avformat_find_stream_info(formatCtx, NULL) < 0)
	{
		printf("find video info failed \n");
		return -1;
	}
	videoIndex = -1;
	for (int i = 0; i < formatCtx->nb_streams; i++)
	{
		//查找媒体类型，0是视频，1是音频
		printf("the codec_type is : %d \n", formatCtx->streams[i]->codec->codec_type);
		if (formatCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO)
		{
			videoIndex = i;
		}
	}
	//获取视频的帧率
	AVStream* stream = formatCtx->streams[videoIndex];
	printf("frame rate is : %d ,den is : %d, framerate is : %d \n",
		stream->avg_frame_rate.num, stream->avg_frame_rate.den, stream->avg_frame_rate.num / stream->avg_frame_rate.den);
	//拿到视频流的信息
	codecCtx = formatCtx->streams[videoIndex]->codec;
	width = codecCtx->width;
	height = codecCtx->height;
	//输出视频分辨率
	printf("the resolution of the video is : %d X %d \n", codecCtx->width, codecCtx->height);
	//默认是1，2表示是H.264，结果也确实是H.264
	printf("the ticks_per_frame is : %d \n", codecCtx->ticks_per_frame);
	//获取编码器id，结果是28，也是对应H.264，说明结果正确，前后一致
	printf("this codec_id is : %d \n", codecCtx->codec_id);
	//结果是0，感觉不对
	printf("bit_rate is : %d \n", codecCtx->bit_rate);
	//输出元数据
	AVDictionaryEntry* entry = NULL;
	printf("-------------meta data------------------\n");
	while (entry = av_dict_get(formatCtx->metadata, "", entry, AV_DICT_IGNORE_SUFFIX))
	{
		printf("key is : %s,value is : %s \n", entry->key, entry->value);
	}
	printf("----------------------------------------\n");
	//得先获取编码器信息，然后才能看更详细的编码信息
	codec = avcodec_find_decoder(codecCtx->codec_id);
	//结果也是H.264
	printf("name of codec is : %s,long name is : %s \n", codec->name, codec->long_name);
	//打开解码器
	avcodec_open2(codecCtx, codec, NULL);

	frame = av_frame_alloc();
	frameYUV = av_frame_alloc();
	//根据分辨率计算一个像素数据YUV420P格式需要的空间，width*height*3/2
	buffer = (unsigned char*)av_malloc(av_image_get_buffer_size(AV_PIX_FMT_YUV420P, width, height, 1));
	//把数据填充到frameYUV中，因为还没有读到数据，应该都是0
	av_image_fill_arrays(frameYUV->data, frameYUV->linesize, buffer, AV_PIX_FMT_YUV420P, width, height, 1);
	//申请一个内存空间，存放packet数据，未解码的视频数据
	packet = (AVPacket*)av_malloc(sizeof(AVPacket));
	printf("------------------video information--------------------\n");
	av_dump_format(formatCtx, videoIndex, filename, 0);
	printf("------------------end----------------------------------\n");
	//也是申请空间的，为下面的sws_scale准备的
	img_convert_ctx = sws_getContext(width, height, codecCtx->pix_fmt, width, height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, NULL, NULL, NULL);
	//新建个文件，存储YUV格式的数据
	FILE* output = fopen("output.yuv", "w");
	//开始读视频流
	while (av_read_frame(formatCtx, packet) >= 0)
	{
		if (packet->stream_index == videoIndex)
		{
			int got_picture = 0;
			//解码函数，把packet中的视频流解码，解码后的数据放到frame中
			int ret = avcodec_decode_video2(codecCtx, frame, &got_picture, packet);
			if (ret < 0)
			{
				printf("decode error \n");
				return -1;
			}
			if (got_picture)
			{
				if (flag < 1)
				{
					saveAsJPEG(frame, width, height, 10);
					flag = 1;
				}
				//看上去很复杂，总体作用呢就是把frame中的数据进行一些处理，然后输出到frameYUV中
				//具体啥处理，目前小白不知道
				sws_scale(img_convert_ctx, (const unsigned char* const*)frame->data, frame->linesize, 0, height,
					frameYUV->data, frameYUV->linesize);
				int line_data_size = width * height;
				//把Y分量写入
				fwrite(frameYUV->data[0], 1, line_data_size, output);
				//把U分量写入
				fwrite(frameYUV->data[1], 1, line_data_size / 4, output);
				//把V分量写入
				fwrite(frameYUV->data[2], 1, line_data_size / 4, output);
			}
		}
		//把packet中的数据清空，方便存下次读的数据
		av_free_packet(packet);
	}

	//flush decoder
	//根据雷神所注释，当上面的av_read_frame退出时，解码器中还存有几帧的数据，需要把这几帧的数据也输出出来
	//方法是通过下面的avcodec_decode_video2直接读取AVFrame，不再向解码器传递AVPacket
	while (1)
	{
		int got_picture = 0;
		int ret = avcodec_decode_video2(codecCtx, frame, &got_picture, packet);
		if (ret > 0 && got_picture)
		{
			sws_scale(img_convert_ctx, (const unsigned char* const*)frame->data,
				frame->linesize, 0, height, frameYUV->data, frameYUV->linesize);
			int line_data_size = width * height;
			//把Y分量写入
			// fwrite(frameYUV->data[0], 1, line_data_size, output);
			//把U分量写入
			// fwrite(frameYUV->data[1], 1, line_data_size / 4, output);
			//把V分量写入
			// fwrite(frameYUV->data[2], 1, line_data_size / 4, output);
		}
		else
		{
			break;
		}
	}

	//关闭写入流
	fclose(output);
	av_frame_free(&frame);
	av_frame_free(&frameYUV);
	avcodec_close(codecCtx);
	avformat_close_input(&formatCtx);

	return 0;
}

#include "audio.h"
#include "queue.h"

using namespace std;

int audio_decode_frame(AVCodecContext* ctx, packet_queue_t* queue, AVFrame* frame) {
	int ret;
	AVPacket* pkt = av_packet_alloc();
	if (!pkt) {
		cout << "ERROR:Alloc AVPacket mem for video decoding failed" << endl;
		return -1;
	}
	while (true)
	{
		while (true)
		{
			ret = avcodec_receive_frame(ctx, frame);
			if (ret >= 0) {
				AVRational tb = { 1,frame->sample_rate };
				if (frame->pts != AV_NOPTS_VALUE) {
					frame->pts = av_rescale_q(frame->pts, ctx->pkt_timebase, tb);
				}
				else {
					cout << "WARNING:No audio pts found" << endl;
				}
				return 1;
			}
			else if (ret == AVERROR_EOF) {
				cout << "INFO:Audio decoder has been flushed" << endl;
				avcodec_flush_buffers(ctx);
				return 0;
			}
			else if (ret == AVERROR(EAGAIN)) {
				//cout << "INFO:Audio decoder need more packet" << endl;
				break;
			}
			else {
				cout << "ERROR:Unknown error for audio decoder" << endl;
				continue;
			}
		}

		if (queue::packet_queue_get(queue, pkt, 1) < 0) {
			return -1;
		}
		//seek
		if (pkt->data == NULL || strcmp((char*)pkt->data, "flush") == 0) {
			cout << "audio decoder-->seek occured" << endl;
			avcodec_flush_buffers(ctx);
		}
		else {
			ret = avcodec_send_packet(ctx, pkt);
			if (ret == AVERROR(EAGAIN)) {
				cout << "ERROR:Both send and receive EAGAIN" << endl;
			}
			av_packet_unref(pkt);
		}
	}
	return ret;
}

int audio_decode_thread(void* arg) {
	play_stat_t* is = (play_stat_t*)arg;
	AVFrame* frame = av_frame_alloc();
	if (!frame) {
		cout << "ERROR:Alloc AVFrame mem for video decoding failed" << endl;
		return -1;
	}
	frame_t* af;

	int got_frame = 0;
	AVRational tb;

	while (true)
	{
		got_frame = audio_decode_frame(is->p_audio_ctx, &is->audio_pkt_queue, frame);
		if (got_frame < 0) {
			av_frame_free(&frame);
			continue;
		}
		tb = { 1,frame->sample_rate };
		af = queue::frame_queue_peek_writeable(&is->audio_frame_queue);
		if (!af) {
			av_frame_free(&frame);
			return -1;
		}
		af->pts = (frame->pts == AV_NOPTS_VALUE) ? NAN : frame->pts * av_q2d(tb);
		af->pos = frame->pkt_pos;
		af->duration = av_q2d({ frame->nb_samples,frame->sample_rate });
		av_frame_move_ref(af->frame, frame);
		queue::frame_queue_push(&is->audio_frame_queue);
	}

	return 0;
}

int audio_resample(play_stat_t* is, int64_t audio_callback_time) {
	int data_size, resampled_data_size;
	int64_t dec_channel_layout;
	av_unused double audio_clock0;
	int wanted_nb_samples;
	frame_t* af = queue::frame_queue_peek_readable(&is->audio_frame_queue);
	if (af == NULL) {
		return -1;
	}
	queue::frame_queue_next(&is->audio_frame_queue);

	data_size = av_samples_get_buffer_size(NULL, af->frame->channels, af->frame->nb_samples, (AVSampleFormat)af->frame->format, 1);
	dec_channel_layout =
		(af->frame->channel_layout && af->frame->channels == av_get_channel_layout_nb_channels(af->frame->channel_layout)) ?
		af->frame->channel_layout : av_get_channel_layout_nb_channels(af->frame->channel_layout);
	wanted_nb_samples = af->frame->nb_samples;

	if (af->frame->format != is->audio_param_src.fmt ||
		dec_channel_layout != is->audio_param_src.channel_layout ||
		af->frame->sample_rate != is->audio_param_src.freq) {
		is->audio_convert_ctx = swr_alloc_set_opts(NULL, is->audio_param.channel_layout, is->audio_param.fmt, is->audio_param.freq,
			af->frame->channel_layout, (AVSampleFormat)af->frame->format, af->frame->sample_rate,
			0, NULL);
		if (!is->audio_convert_ctx || swr_init(is->audio_convert_ctx) < 0) {
			cout << "ERROR:Cannot create resample context" << endl;
			swr_free(&is->audio_convert_ctx);
			return -1;
		}
		is->audio_param_src.channel_layout = dec_channel_layout;
		is->audio_param_src.channels = af->frame->channels;
		is->audio_param_src.freq = af->frame->sample_rate;
		is->audio_param_src.fmt = (AVSampleFormat)af->frame->format;
	}

	if (is->audio_convert_ctx) {
		const uint8_t** in = (const uint8_t**)af->frame->extended_data;
		uint8_t** out = &is->p_audio_frame_resample;
		int out_count = (int64_t)wanted_nb_samples * is->audio_param.freq / af->frame->sample_rate + 256;
		int out_size = av_samples_get_buffer_size(NULL, is->audio_param.channels, out_count, is->audio_param.fmt, 0);
		av_fast_malloc(&is->p_audio_frame_resample, &is->audio_frame_resample_size, out_size);
		if (!&is->p_audio_frame_resample) {
			cout << "ERROR:Alloc AVFrame for audio resampling failed" << endl;
			return -1;
		}
		int tmp = swr_convert(is->audio_convert_ctx, out, out_count, in, af->frame->nb_samples);
		if (tmp < 0) {
			cout << "ERROR:Resample failed" << endl;
			return -1;
		}
		if (tmp == out_count) {
			cout << "WARNING:Audio buffer probably too small" << endl;
			if (swr_init(is->audio_convert_ctx) < 0) {
				swr_free(&is->audio_convert_ctx);
			}
		}
		is->p_audio_frame = is->p_audio_frame_resample;
		resampled_data_size = tmp * is->audio_param.channels * av_get_bytes_per_sample(is->audio_param.fmt);
	}
	else
	{
		is->p_audio_frame = af->frame->data[0];
		resampled_data_size = data_size;
	}

	audio_clock0 = is->audio_clock;
	if (!isnan(af->pts)) {
		is->audio_clock = af->pts + static_cast<double>(af->frame->nb_samples) / af->frame->sample_rate;
	}
	else {
		is->audio_clock = NAN;
	}
	is->audio_clock_serial = af->serial;
	static double last_clock;
	//cout << "INFO:Audio:delay=" << is->audio_clock - last_clock << ",clock=" << is->audio_clock << ",clock0=" << audio_clock0 << endl;
	last_clock = is->audio_clock;
	return resampled_data_size;

}

void sdl_audio_callback(void* udata, Uint8* stream, int len) {
	play_stat_t* is = (play_stat_t*)udata;
	int audio_size, tmp;
	int64_t audio_callback_time = av_gettime_relative();
	while (len > 0)
	{
		if (is->audio_cp_index >= (int)is->audio_frame_size) {
			audio_size = audio_resample(is, audio_callback_time);
			if (audio_size < 0) {
				is->p_audio_frame = NULL;
				is->audio_frame_size = SDL_MIN_AUDIO_BUFFER_SIZE / is->audio_param.frame_size * is->audio_param.frame_size;
			}
			else {
				is->audio_frame_size = audio_size;
			}
			is->audio_cp_index = 0;
		}

		tmp = is->audio_frame_size - is->audio_cp_index;
		if (tmp > len) {
			tmp = len;
		}
		if (is->p_audio_frame != NULL) {
			memcpy(stream, (uint8_t*)is->p_audio_frame + is->audio_cp_index, tmp);
		}
		else {
			memset(stream, 0, tmp);
		}
		len -= tmp;
		stream += tmp;
		is->audio_cp_index += tmp;
	}

	is->audio_write_buf_size = is->audio_frame_size - is->audio_cp_index;
	if (!isnan(is->audio_clock)) {
		player::set_clock_at(&is->audio_clk,
			//已经有一份数据拷贝到了声卡缓冲区，所以在计算音频时钟时要将缓冲区的数据也算上，故要加上(通道数*声卡缓冲区大小)
			is->audio_clock - static_cast<double>(2 * is->audio_hw_buf_size + is->audio_write_buf_size) / is->audio_param.bytes_per_sec,
			is->audio_clock_serial, audio_callback_time / 1000000.0);
	}

}

int audio::open_audio(play_stat_t* is)
{
	open_audio_stream(is);
	open_audio_playing(is);
	return 0;
}

int audio::open_audio_playing(void* arg)
{
	play_stat_t* is = (play_stat_t*)arg;
	SDL_AudioSpec spec;

	spec.freq = is->p_audio_ctx->sample_rate;
	spec.format = AUDIO_S16SYS;
	spec.channels = is->p_audio_ctx->channels;
	spec.samples = FFMAX(SDL_MIN_AUDIO_BUFFER_SIZE, 2 << av_log2(spec.freq / SDL_MAX_AUDIO_CALLBACK_PER_SEC));
	spec.callback = sdl_audio_callback;
	spec.userdata = is;

	if (SDL_OpenAudio(&spec, NULL) < 0) {
		cout << "ERROR:Open audio device failed:" << SDL_GetError() << endl;
		return -1;
	}

	is->audio_param.fmt = AV_SAMPLE_FMT_S16;
	is->audio_param.freq = spec.freq;
	is->audio_param.channel_layout = av_get_default_channel_layout(spec.channels);
	is->audio_param.channels = spec.channels;
	is->audio_param.bytes_per_sec = av_samples_get_buffer_size(NULL, spec.channels, spec.freq, AV_SAMPLE_FMT_S16, 1);
	is->audio_param.frame_size = av_samples_get_buffer_size(NULL, spec.channels, 1, AV_SAMPLE_FMT_S16, 1);
	is->audio_param_src = is->audio_param;
	is->audio_hw_buf_size = spec.size;
	is->audio_frame_size = 0;
	is->audio_cp_index = 0;
	SDL_PauseAudio(0);

	return 0;
}

int audio::open_audio_stream(play_stat_t* is)
{
	int ret;

	AVCodec* audio_decoder = NULL;
	AVCodecContext* audio_ctx = NULL;

	audio_decoder = avcodec_find_decoder(is->p_audio_stream->codecpar->codec_id);
	if (!audio_decoder) {
		cout << "ERROR:Find audio decoder failed" << endl;
		return -1;
	}

	audio_ctx = avcodec_alloc_context3(audio_decoder);
	if (!audio_ctx) {
		cout << "ERROR:Alloc audio context failed" << endl;
		return -1;
	}

	ret = avcodec_parameters_to_context(audio_ctx, is->p_audio_stream->codecpar);
	if (ret < 0) {
		cout << "ERROR:Copy audio param to context failed" << endl;
		return -1;
	}

	ret = avcodec_open2(audio_ctx, audio_decoder, NULL);
	if (ret < 0) {
		cout << "ERROR:Open audio decoder failed" << endl;
		return -1;
	}

	audio_ctx->pkt_timebase = is->p_audio_stream->time_base;
	is->p_audio_ctx = audio_ctx;

	SDL_CreateThread(audio_decode_thread, "audio decode thread", is);

	return 0;
}

#include "demux.h"
#include "queue.h"

using namespace std;

int stream_has_enough_packets(AVStream* st, int stream_id, packet_queue_t* queue) {
	return stream_id < 0 ||
		queue->abort_request ||
		/*
		AV_DISPOSITION_ATTACHED_PIC 是一个标志
		如果一个流中含有这个标志的话，那么就是说这个流是 *.mp3 文件中的一个 Video Stream
		并且该流只有一个 AVPacket ，也就是 attached_pic
		这个 AVPacket 中所存储的内容就是这个 *.mp3 文件的封面图片
		它只存放了封面信息，在播放或者导出时，不需要这个数据
		因此可以使用这个标志很好的区分这个特殊的 Video Stream
		并且通过判断，屏蔽该流，不对其进行操作
		*/
		(st->disposition & AV_DISPOSITION_ATTACHED_PIC) ||
		(queue->nb_packets > MIN_FRAMES && (!queue->duration || av_q2d(st->time_base) * queue->duration > 1.0));
}

int demux_thread(void* arg) {
	play_stat_t* is = (play_stat_t*)arg;
	AVFormatContext* ifmt_ctx = is->p_ifmt_ctx;
	int ret;
	AVPacket* pkt = av_packet_alloc();
	//TODO

	SDL_mutex* mutex = SDL_CreateMutex();
	cout << "INFO:Demux beginning-----------" << endl;

	while (true)
	{
		if (is->abort_request) {
			break;
		}

		// seek
		if (is->seek_req) {
			int seek_flags = is->seek_flags;
			int64_t seek_pos = is->seek_pos;
			int64_t seek_rel = is->seek_rel;

			auto min_ts = (seek_rel > 0) ? (seek_pos - seek_rel + 2) : (INT64_MIN);
			auto max_ts= (seek_rel < 0) ? (seek_pos - seek_rel - 2) : (INT64_MAX);
			ret = avformat_seek_file(is->p_ifmt_ctx, -1, min_ts, seek_pos, max_ts, seek_flags);
			//TODO 

			if (is->audio_index >= 0) {
				queue::packet_queue_flush(&is->audio_pkt_queue);
				queue::packet_queue_put_flushpacket(&is->audio_pkt_queue, is->audio_index);
			}

			if (is->video_index >= 0) {
				queue::packet_queue_flush(&is->video_pkt_queue);
				queue::packet_queue_put_flushpacket(&is->video_pkt_queue, is->video_index);
			}
			player::set_clock(&is->audio_clk, seek_pos / (double)AV_TIME_BASE, is->audio_clk.serial);
			is->seek_req = false;

		}

		if (is->audio_pkt_queue.size + is->video_pkt_queue.size > MAX_QUEUE_SIZE ||
			(stream_has_enough_packets(is->p_audio_stream, is->audio_index, &is->audio_pkt_queue)) &&
			stream_has_enough_packets(is->p_video_stream, is->video_index, &is->video_pkt_queue)
			) {
			//cout << "INFO:Packet queue has enough packets,wait for playing..." << endl;
			SDL_LockMutex(mutex);
			SDL_CondWaitTimeout(is->continue_read_thread, mutex, 10);
			SDL_UnlockMutex(mutex);
			continue;
		}

		ret = av_read_frame(ifmt_ctx, pkt);
		if (ret < 0) {
			if (ret == AVERROR_EOF) {
				if (is->video_index > 0) {
					queue::packet_queue_put_nullpacket(&is->video_pkt_queue, is->video_index);
				}
				if (is->audio_index > 0) {
					queue::packet_queue_put_nullpacket(&is->audio_pkt_queue, is->audio_index);
				}
			}

			SDL_LockMutex(mutex);
			SDL_CondWaitTimeout(is->continue_read_thread, mutex, 10);
			SDL_UnlockMutex(mutex);
			continue;
		}

		if (pkt->stream_index == is->audio_index) {
			queue::packet_queue_put(&is->audio_pkt_queue, pkt);
		}
		else if (pkt->stream_index == is->video_index) {
			queue::packet_queue_put(&is->video_pkt_queue, pkt);
		}
		else {
			av_packet_unref(pkt);
		}
	}

	SDL_DestroyMutex(mutex);
	return 0;
}

int demux::open_demux(play_stat_t* is)
{
	if (demux_init(is) < 0) {
		cout << "ERROR:Demux init failed" << endl;
		return -1;
	}
	is->read_thread = SDL_CreateThread(demux_thread, "demux thread", is);
	if (!is->read_thread) {
		cout << "ERROR:Create demux thread failed:" << SDL_GetError() << endl;
		return -1;
	}
	return 0;
}

int demux::demux_init(play_stat_t* is)
{
	int ret, v_idx = -1, a_idx = -1;
	AVFormatContext* ifmt_ctx = NULL;
	avformat_open_input(&ifmt_ctx, is->filename, NULL, NULL);
	if (!ifmt_ctx) {
		cout << "ERROR:Open input file failed" << endl;
		return -1;
	}

	ret = avformat_find_stream_info(ifmt_ctx, NULL);
	if (ret < 0) {
		cout << "ERROR:Find stream info failed" << endl;
		avformat_close_input(&ifmt_ctx);
		return -1;
	}

	for (int i = 0; i < (int)ifmt_ctx->nb_streams; i++) {
		if (ifmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO) {
			a_idx = i;
			cout << "INFO:Find a audio stream" << endl;
		}
		if (ifmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
			v_idx = i;
			cout << "INFO:Find a video stream" << endl;
		}
	}

	is->p_ifmt_ctx = ifmt_ctx;
	is->video_index = v_idx;
	is->audio_index = a_idx;
	is->p_video_stream = ifmt_ctx->streams[v_idx];
	is->p_audio_stream = ifmt_ctx->streams[a_idx];

	return 0;
}

int demux::demux_deinit()
{
	return 0;
}

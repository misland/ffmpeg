#include <iostream>
#include "player.h"
#include "queue.h"
#include "demux.h"
#include "audio.h"
#include "video.h"
using namespace std;

int player::play(const char* input)
{
	demux d;
	audio a;
	video v;
	if (player_init(input) < 0) {
		cout << "ERROR:Initialize player failed" << endl;
		return -1;
	}
	d.open_demux(is);
	v.open_video(is);
	a.open_audio(is);

	SDL_Event event;
	while (true)
	{
		if (is->audio_pkt_queue.nb_packets <= 0 &&
			is->video_pkt_queue.nb_packets <= 0 &&
			is->audio_frame_queue.size <= 1 &&
			is->video_frame_queue.size <= 1) {
			cout << "play to end" << endl;
			SDL_Event tmp;
			tmp.type = SDL_QUIT;
			SDL_PushEvent(&tmp);
		}
		//SDL_WaitEvent(&event);
		while (SDL_PollEvent(&event)) {
			switch (event.type)
			{
			case SDL_QUIT:
				cout << "close event" << endl;
				is->quit = 1;
				player_deinit();
				break;
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_SPACE) {
					cout << "space down event" << endl;
					toggle_pause();
				}
				if (event.key.keysym.sym == SDLK_ESCAPE) {
					cout << "escape event" << endl;
					is->quit = 1;
					player_deinit();
				}
				if (event.key.keysym.sym == SDLK_RIGHT) {
					cout << "right key pressed" << endl;
					doSeek(0);
				}
				if (event.key.keysym.sym == SDLK_LEFT) {
					cout << "left key pressed" << endl;
					doSeek(1);
				}
				break;
			default:
				break;
			}
		}
	}


	return 0;
}

void player::stream_toggle_pause()
{
	if (is->paused) {
		SDL_PauseAudio(0);
		is->frame_timer += av_gettime_relative() / 1000000.0 - is->video_clk.last_updated;
		set_clock(&is->video_clk, get_clock(&is->video_clk), is->video_clk.serial);
		set_clock(&is->audio_clk, get_clock(&is->audio_clk), is->audio_clk.serial);
	}
	else
	{
		SDL_PauseAudio(1);
	}
	is->paused = is->audio_clk.paused = is->video_clk.paused = !is->paused;
}

void player::toggle_pause()
{
	stream_toggle_pause();
	is->step = 0;
}

double player::get_clock(play_clock_t* c)
{
	if (*c->queue_serial != c->serial) {
		return NAN;
	}
	if (c->paused) {
		return c->pts;
	}
	else {
		double time = av_gettime_relative() / 1000000.0;
		double ret = c->pts_drift + time;
		return ret;
	}
	return 0.0;
}

void player::doSeek(int orientation)
{
	auto current_ts = get_clock(&is->video_clk);
	cout << "audio clock:" << current_ts << endl;
	current_ts = orientation == 0 ? current_ts += 5.0 : current_ts -= 5.0;
	is->seek_rel = (int64_t)(5.0 * AV_TIME_BASE);
	is->seek_pos = (int64_t)(current_ts * AV_TIME_BASE);
	is->seek_flags = orientation;
	is->seek_req = true;
}

void player::set_clock_at(play_clock_t* c, double pts, int serial, double time)
{
	c->pts = pts;
	c->last_updated = time;
	c->pts_drift = c->pts - time;
	c->serial = serial;
}

void player::set_clock(play_clock_t* c, double pts, int serial)
{
	double time = av_gettime_relative() / 1000000.0;
	set_clock_at(c, pts, serial, time);
}

int player::player_init(const char* input)
{
	is = (play_stat_t*)av_mallocz(sizeof(play_stat_t));
	if (!is) {
		return -1;
	}
	is->filename = input;

	if (queue::frame_queue_init(&is->audio_frame_queue, &is->audio_pkt_queue, SAMPLE_QUEUE_SIZE, 1) < 0 ||
		queue::frame_queue_init(&is->video_frame_queue, &is->video_pkt_queue, VIDEO_PICTURE_QUEUE_SIZE, 1) < 0) {
		player_deinit();
	}

	if (queue::packet_queue_init(&is->audio_pkt_queue, 0) < 0 ||
		queue::packet_queue_init(&is->video_pkt_queue, 1) < 0) {
		player_deinit();
	}

	AVPacket flush_pkt;
	flush_pkt.data = NULL;

	if (queue::packet_queue_put(&is->video_pkt_queue, &flush_pkt) < 0 ||
		queue::packet_queue_put(&is->audio_pkt_queue, &flush_pkt) < 0) {
		player_deinit();
	}

	if (!(is->continue_read_thread = SDL_CreateCond())) {
		cout << "ERROR:Create continue_read_thread failed:" << SDL_GetError << endl;
		player_deinit();
	}

	init_clock(&is->video_clk, &is->video_pkt_queue.serial);
	init_clock(&is->audio_clk, &is->audio_pkt_queue.serial);
	is->abort_request = 0;

	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER)) {
		cout << "ERROR:Init SDL failed:" << SDL_GetError() << endl;
		player_deinit();
	}
	return 0;
}

void player::player_deinit()
{
	is->abort_request = 1;
	SDL_WaitThread(is->read_thread, NULL);

	if (is->p_ifmt_ctx) {
		avformat_close_input(&is->p_ifmt_ctx);
	}
	queue::packet_queue_abort(&is->audio_pkt_queue);
	queue::packet_queue_abort(&is->video_pkt_queue);
	queue::packet_queue_destroy(&is->audio_pkt_queue);
	queue::packet_queue_destroy(&is->video_pkt_queue);

	queue::frame_queue_destroy(&is->audio_frame_queue);
	queue::frame_queue_destroy(&is->video_frame_queue);

	SDL_DestroyCond(is->continue_read_thread);
	if (is->video_convert_ctx) {
		sws_freeContext(is->video_convert_ctx);
	}
	if (is->audio_convert_ctx) {
		swr_free(&is->audio_convert_ctx);
	}
	if (is->sdl_video.texture) {
		SDL_DestroyTexture(is->sdl_video.texture);
	}
	if (is->sdl_video.render) {
		SDL_DestroyRenderer(is->sdl_video.render);
	}
	if (is->sdl_video.window) {
		SDL_DestroyWindow(is->sdl_video.window);
	}
	SDL_CloseAudio();
	SDL_Quit();
	av_free(is);
	exit(0);

}

void player::init_clock(play_clock_t* c, int* queue_serial)
{
	c->speed = 1.0;
	c->paused = 0;
	c->queue_serial = queue_serial;
	set_clock(c, NAN, -1);
}

void player::do_exit(play_stat_t* is)
{
}

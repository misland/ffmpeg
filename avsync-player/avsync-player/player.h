#pragma once
#include <iostream>

extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/frame.h>
#include <libavutil/time.h>
#include <libavutil/imgutils.h>
#include <SDL2/SDL.h>
}

#define AV_SYNC_THRESHOLD_MIN 0.04
#define AV_SYNC_THRESHOLD_MAX 0.1
#define AV_SYNC_FRAMEDUMP_THRESHOLD 0.1
#define AV_NOSYNC_THRESHOLD 10.0

#define REFRESH_RATE 0.01

#define SDL_AUDIO_BUFFER_SIZE 1024
#define SDL_MAX_AUDIO_FRAME_SIZE 19200
#define SDL_MIN_AUDIO_BUFFER_SIZE 512
#define SDL_MAX_AUDIO_CALLBACK_PER_SEC 30

#define MAX_QUEUE_SIZE (15*1024*1024)
#define MIN_FRAMES 25

#define VIDEO_PICTURE_QUEUE_SIZE 3
#define SUBPICTURE_QUEUE_SIZE 16
#define SAMPLE_QUEUE_SIZE 9
#define FRAME_QUEUE_SIZE FFMAX(SAMPLE_QUEUE_SIZE, FFMAX(VIDEO_PICTURE_QUEUE_SIZE,SUBPICTURE_QUEUE_SIZE))

#define FF_QUIT_EVENT (SDL_USEREVENT + 2)

typedef struct {
	double pts;
	double pts_drift;
	double last_updated;
	double speed;
	int serial;
	int paused;
	int* queue_serial;
} play_clock_t;

typedef struct {
	int freq;
	int channels;
	int64_t channel_layout;
	enum AVSampleFormat fmt;
	int frame_size;
	int bytes_per_sec;
} audio_param_t;

typedef struct {
	SDL_Window* window;
	SDL_Renderer* render;
	SDL_Texture* texture;
	SDL_Rect rect;
} sdl_video_t;

typedef struct {
	AVPacketList* first_pkt, * last_pkt;
	int nb_packets;
	int size;
	int64_t duration;
	int abort_request;
	int serial;
	SDL_mutex* mutex;
	SDL_cond* cond;
	int type;
} packet_queue_t;

typedef struct {
	AVFrame* frame;
	int serial;
	double pts, duration;
	int64_t pos;
	int width, height, format, uploaded, flip_v;
	AVRational sar;
} frame_t;

typedef struct {
	frame_t queue[FRAME_QUEUE_SIZE];
	int rindex, //队列中的读索引
		windex, //写索引
		size,
		max_size,
		keep_last,
		rindex_shown;//队列中的帧是否已开始播放
	SDL_mutex* mutex;
	SDL_cond* cond;
	packet_queue_t* pktq;
} frame_queue_t;

typedef struct {
	const char* filename;
	AVFormatContext* p_ifmt_ctx;
	AVStream* p_audio_stream;
	AVStream* p_video_stream;
	AVCodecContext* p_audio_ctx;
	AVCodecContext* p_video_ctx;

	int audio_index, video_index;
	sdl_video_t sdl_video;

	play_clock_t audio_clk;
	play_clock_t video_clk;
	double frame_timer;

	packet_queue_t audio_pkt_queue;
	packet_queue_t video_pkt_queue;

	frame_queue_t audio_frame_queue;
	frame_queue_t video_frame_queue;

	struct SwrContext* audio_convert_ctx;
	struct SwsContext* video_convert_ctx;
	AVFrame* p_frame_yuv;

	audio_param_t audio_param;
	audio_param_t audio_param_src;
	int audio_hw_buf_size;
	uint8_t* p_audio_frame;
	uint8_t* p_audio_frame_resample;
	unsigned int audio_frame_size;
	unsigned int audio_frame_resample_size;
	int audio_cp_index;
	int audio_write_buf_size;
	double audio_clock;
	int audio_clock_serial;

	int abort_request;
	int paused, step, quit;

	SDL_cond* continue_read_thread;
	SDL_Thread* read_thread;

	int seek_req;
	int64_t seek_rel;
	int64_t seek_pos;
	int seek_flags;

} play_stat_t;

static play_stat_t* is;
class player
{
public:
	int play(const char* input);
	void toggle_pause();
	void doSeek(int orientation);
	static double get_clock(play_clock_t* c);
	static void set_clock_at(play_clock_t* c, double pts, int serial, double time);
	static void set_clock(play_clock_t* c, double pts, int serial);
	static void set_clock_speed(play_clock_t* c, double speed);
	static void sync_play_clock_to_slave(play_clock_t* c, play_clock_t* slave);
	static void stream_toggle_pause(play_stat_t* is);
	static void toggle_pause(play_stat_t* is);


private:
	int player_init(const char* input);
	void player_deinit();
	void init_clock(play_clock_t* c, int* queue_serial);
	void do_exit(play_stat_t* is);
	void stream_toggle_pause();
};


#include "queue.h"

using namespace std;

int queue::packet_queue_init(packet_queue_t* q, int type)
{
	memset(q, 0, sizeof(packet_queue_t));
	if (!(q->mutex = SDL_CreateMutex())) {
		cout << "ERROR:Create packet queue mutex failed:" << SDL_GetError() << endl;
		return AVERROR(ENOMEM);
	}
	if (!(q->cond = SDL_CreateCond())) {
		cout << "ERROR:Create packet queue cond failed:" << SDL_GetError() << endl;
		return AVERROR(ENOMEM);
	}
	q->abort_request = 0;
	q->type = type;

	return 0;
}

int queue::packet_queue_put(packet_queue_t* q, AVPacket* pkt)
{
	AVPacketList* node;
	if (av_packet_make_refcounted(pkt) < 0) {
		cout << "ERROR:Add packet reference failed" << endl;
		return -1;
	}
	node = (AVPacketList*)av_malloc(sizeof(AVPacketList));
	if (!node) {
		cout << "ERROR:Alloc for AVPacketList failed" << endl;
		return -1;
	}
	node->pkt = *pkt;
	node->next = NULL;
	SDL_LockMutex(q->mutex);
	if (!q->last_pkt) {
		q->first_pkt = node;
	}
	else {
		q->last_pkt->next = node;
	}
	q->last_pkt = node;
	q->nb_packets++;
	q->size += pkt->size;
	SDL_CondSignal(q->cond);
	SDL_UnlockMutex(q->mutex);
	return 0;
}

int queue::packet_queue_get(packet_queue_t* q, AVPacket* pkt, int block)
{
	AVPacketList* node;
	int ret;
	SDL_LockMutex(q->mutex);
	while (true)
	{
		node = q->first_pkt;
		if (node) {
			q->first_pkt = node->next;
			if (!q->first_pkt) {
				q->last_pkt = NULL;
			}
			q->nb_packets--;
			q->size -= node->pkt.size;
			*pkt = node->pkt;
			av_free(node);
			ret = 1;
			break;
		}
		else if (!block) {
			cout << "no packet in queue,waiting..." << endl;
			ret = 0;
			break;
		}
		else {
			SDL_CondWait(q->cond, q->mutex);
			continue;
		}
	}
	SDL_UnlockMutex(q->mutex);
	return ret;
}

void queue::packet_queue_abort(packet_queue_t* q)
{
	SDL_LockMutex(q->mutex);
	q->abort_request = 1;
	SDL_CondSignal(q->cond);
	SDL_UnlockMutex(q->mutex);
}

void queue::packet_queue_destroy(packet_queue_t* q)
{
	packet_queue_flush(q);
	SDL_DestroyMutex(q->mutex);
	SDL_DestroyCond(q->cond);
}

void queue::packet_queue_flush(packet_queue_t* q)
{
	AVPacketList* pkt, * pkt1;
	SDL_LockMutex(q->mutex);
	for (pkt = q->first_pkt; pkt; pkt = pkt1) {
		pkt1 = pkt->next;
		av_packet_unref(&pkt->pkt);
		//av_free(&pkt);
	}
	q->last_pkt = NULL;
	q->first_pkt = NULL;
	q->nb_packets = 0;
	q->size = 0;
	q->duration = 0;
	SDL_UnlockMutex(q->mutex);
}

void queue::packet_queue_put_nullpacket(packet_queue_t* q, int stream_index)
{
	AVPacket* pkt = av_packet_alloc();
	//TODO
	pkt->data = NULL;
	pkt->size = 0;
	pkt->stream_index = stream_index;
	packet_queue_put(q, pkt);
}

void queue::packet_queue_put_flushpacket(packet_queue_t* q, int stream_index)
{
	AVPacket* pkt = av_packet_alloc();
	//TODO

	const char* FLUSH_DATA = "flush";
	av_new_packet(pkt, strlen(FLUSH_DATA));
	copy_n(FLUSH_DATA, strlen(FLUSH_DATA), pkt->data);
	pkt->stream_index = stream_index;
	packet_queue_put(q, pkt);
}

int queue::frame_queue_init(frame_queue_t* f, packet_queue_t* pktq, int max_size, int keep_last)
{
	int i;
	memset(f, 0, sizeof(frame_queue_t));
	if (!(f->mutex = SDL_CreateMutex())) {
		cout << "ERROR:Create frame queue mutex failed:" << SDL_GetError() << endl;
		return AVERROR(ENOMEM);
	}
	if (!(f->cond = SDL_CreateCond())) {
		cout << "ERROR:Create frame queue cond failed:" << SDL_GetError() << endl;
		return AVERROR(ENOMEM);
	}

	f->pktq = pktq;
	f->max_size = FFMIN(max_size, FRAME_QUEUE_SIZE);
	f->keep_last = !!keep_last;
	for (i = 0; i < f->max_size; i++) {
		if (!(f->queue[i].frame = av_frame_alloc())) {
			return AVERROR(ENOMEM);
		}
	}
	return 0;
}

void queue::frame_queue_destroy(frame_queue_t* q)
{
	for (int i = 0; i < q->max_size; i++) {
		frame_t* item = &q->queue[i];
		av_frame_unref(item->frame);
		av_frame_free(&item->frame);
	}
	SDL_DestroyMutex(q->mutex);
	SDL_DestroyCond(q->cond);
}

frame_t* queue::frame_queue_peek_writeable(frame_queue_t* f)
{
	SDL_LockMutex(f->mutex);
	while (f->size >= f->max_size && !f->pktq->abort_request)
	{
		SDL_CondWait(f->cond, f->mutex);
	}
	SDL_UnlockMutex(f->mutex);

	if (f->pktq->abort_request) {
		return NULL;
	}
	return &f->queue[f->windex];
}

frame_t* queue::frame_queue_peek_readable(frame_queue_t* f)
{
	SDL_LockMutex(f->mutex);
	while (f->size - f->rindex_shown <= 0 && !f->pktq->abort_request)
	{
		SDL_CondWait(f->cond, f->mutex);
	}
	SDL_UnlockMutex(f->mutex);
	if (f->pktq->abort_request) {
		return NULL;
	}

	return &f->queue[(f->rindex + f->rindex_shown) % f->max_size];
}

frame_t* queue::frame_queue_peek_last(frame_queue_t* f)
{
	return &f->queue[f->rindex];
}

frame_t* queue::frame_queue_peek(frame_queue_t* f)
{
	return &f->queue[(f->rindex + f->rindex_shown) % f->max_size];
}

frame_t* queue::frame_queue_peek_next(frame_queue_t* f)
{
	return &f->queue[(f->rindex + f->rindex_shown + 1) % f->max_size];
}

void queue::frame_queue_next(frame_queue_t* f)
{
	if (f->keep_last && !f->rindex_shown) {
		f->rindex_shown = 1;
		return;
	}
	av_frame_unref(f->queue[f->rindex].frame);
	if (++f->rindex == f->max_size) {
		f->rindex = 0;
	}
	SDL_LockMutex(f->mutex);
	f->size--;
	SDL_CondSignal(f->cond);
	SDL_UnlockMutex(f->mutex);
}

frame_t* queue::frame_queue_last(frame_queue_t* f)
{
	return &f->queue[f->rindex];
}

void queue::frame_queue_push(frame_queue_t* f)
{
	if (++f->windex == f->max_size) {
		f->windex = 0;
	}
	SDL_LockMutex(f->mutex);
	f->size++;
	SDL_CondSignal(f->cond);
	SDL_UnlockMutex(f->mutex);
}

int queue::frame_queue_nb_remaining(frame_queue_t* f)
{
	return f->size - f->rindex_shown;
}

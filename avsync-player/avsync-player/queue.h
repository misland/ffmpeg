#pragma once
#include "player.h"
class queue
{
public:
	static int frame_queue_init(frame_queue_t* f, packet_queue_t* pktq, int max_size, int keep_last);

	static void frame_queue_destroy(frame_queue_t* q);

	static frame_t* frame_queue_peek_writeable(frame_queue_t* f);

	static frame_t* frame_queue_peek_readable(frame_queue_t* f);

	static frame_t* frame_queue_peek_last(frame_queue_t* f);

	static frame_t* frame_queue_peek(frame_queue_t* f);

	static frame_t* frame_queue_peek_next(frame_queue_t* f);

	static void frame_queue_next(frame_queue_t* f);

	static frame_t* frame_queue_last(frame_queue_t* f);

	static void frame_queue_push(frame_queue_t* f);

	static int frame_queue_nb_remaining(frame_queue_t* f);

	static int packet_queue_init(packet_queue_t* q, int type);

	static int packet_queue_put(packet_queue_t* q, AVPacket* pkt);

	static int packet_queue_get(packet_queue_t* q, AVPacket* pkt, int block);

	static void packet_queue_abort(packet_queue_t* q);

	static void packet_queue_destroy(packet_queue_t* q);

	static void packet_queue_flush(packet_queue_t* q);

	static void packet_queue_put_nullpacket(packet_queue_t* q, int stream_index);

	static void packet_queue_put_flushpacket(packet_queue_t* q, int stream_index);
};


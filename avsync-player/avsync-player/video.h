#pragma once
#include "player.h"
#include "queue.h"
class video
{
public:
	int open_video(play_stat_t* is);
private:
	int open_video_stream(play_stat_t* is);
	int open_video_playing(play_stat_t* is);
};


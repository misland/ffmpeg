#include "video.h"

using namespace std;

void update_video_pts(play_stat_t* is, double pts, int64_t pos, int serial) {
	player::set_clock(&is->video_clk, pts, serial);
}

double vp_duration(play_stat_t* is, frame_t* vp, frame_t* nextvp) {
	if (vp->serial == nextvp->serial) {
		double duration = nextvp->pts - vp->pts;
		if (isnan(duration) || duration <= 0) {
			return vp->duration;
		}
		else {
			return duration;
		}
	}
	else {
		return 0.0;
	}
}

double compute_target_delay(double delay, play_stat_t* is) {
	double sync_threshold, diff = 0.0;

	diff = player::get_clock(&is->video_clk) - player::get_clock(&is->audio_clk);//如果为负，说明视频落后音频

	sync_threshold = FFMAX(AV_SYNC_THRESHOLD_MIN, FFMIN(AV_SYNC_THRESHOLD_MAX, delay));//delay是上一帧播放时长，这行结果肯定是正值
	if (!isnan(diff)) {
		if (diff <= -sync_threshold) {//说明diff为负
			delay = FFMAX(0, delay + diff);//视频落后的时间+此帧播放时间依然小于0，立刻播放此帧
		}
		else if (diff >= sync_threshold && delay > AV_SYNC_FRAMEDUMP_THRESHOLD) {//视频超过音频，并且上帧播放时长超过100ms了
			delay = delay + diff;//上一帧播放时间太长了，不再加倍了
		}
		else if (diff >= sync_threshold) {
			delay = delay * 2;//视频播放超前音频，需要等待音频
		}
	}

	//cout << "INFO:Video-->delay=" << delay << " A-V=" << -diff << endl;
	return delay;
}

void video_display(play_stat_t* is) {
	frame_t* vp;
	vp = queue::frame_queue_last(&is->video_frame_queue);
	sws_scale(is->video_convert_ctx,
		(const uint8_t* const*)vp->frame->data,
		vp->frame->linesize,
		0,
		is->p_video_ctx->height,
		is->p_frame_yuv->data,
		is->p_frame_yuv->linesize);

	SDL_UpdateYUVTexture(is->sdl_video.texture,
		&is->sdl_video.rect,
		is->p_frame_yuv->data[0],
		is->p_frame_yuv->linesize[0],
		is->p_frame_yuv->data[1],
		is->p_frame_yuv->linesize[1],
		is->p_frame_yuv->data[2],
		is->p_frame_yuv->linesize[2]);

	SDL_RenderClear(is->sdl_video.render);
	SDL_RenderCopy(is->sdl_video.render, is->sdl_video.texture, NULL, &is->sdl_video.rect);
	SDL_RenderPresent(is->sdl_video.render);
}

void video_refresh(void* udata, double* remaining_time) {
	play_stat_t* is = (play_stat_t*)udata;
	double time;
	static bool first_frame = true;
retry:
	if (queue::frame_queue_nb_remaining(&is->video_frame_queue) == 0) {
		return;
	}
	double last_duration, duration, delay;
	frame_t* vp, * lastvp;

	lastvp = queue::frame_queue_peek_last(&is->video_frame_queue);//取出读索引对应的数据（方法名是取上一帧，其实是读索引对应的）
	vp = queue::frame_queue_peek(&is->video_frame_queue);//peek到下一帧
	if (first_frame) {
		is->frame_timer = av_gettime_relative() / 1000000.0;
		first_frame = false;
	}

	if (is->paused) {
		goto display;
	}

	last_duration = vp_duration(is, lastvp, vp);//计算上一帧和当前帧之间的pts时间差，也就是上一帧播放时间差
	delay = compute_target_delay(last_duration, is);//计算视频与音频的时间差，决定是立刻播放视频帧还是等待

	time = av_gettime_relative() / 1000000.0;//这个应该是程序时钟
	if (time < is->frame_timer + delay) {//大于程序时钟，说明还不该播放要等待
		*remaining_time = FFMIN(is->frame_timer + delay - time, *remaining_time);//看超前的时间是否大于最小阀值（10ms）
		return;
	}

	//需要播放
	is->frame_timer += delay;
	if (delay > 0 && //frame_timer落后程序时钟太久（超过100ms），将其更新为程序时钟
		time - is->frame_timer > AV_SYNC_THRESHOLD_MAX) {
		is->frame_timer = time;
	}

	SDL_LockMutex(is->video_frame_queue.mutex);
	if (!isnan(vp->pts)) {
		update_video_pts(is, vp->pts, vp->pos, vp->serial);//更新视频时钟
	}
	SDL_UnlockMutex(is->video_frame_queue.mutex);

	if (queue::frame_queue_nb_remaining(&is->video_frame_queue) > 1) {//未播放的视频帧比较多
		frame_t* next = queue::frame_queue_peek_next(&is->video_frame_queue);
		duration = vp_duration(is, vp, next);
		if (time > is->frame_timer + duration) {
			queue::frame_queue_next(&is->video_frame_queue);//剩余未播放的帧大于1帧，并且下一帧播放时间仍然赶不上程序时钟，就抛弃不显示了
			goto retry;
		}
	}

	queue::frame_queue_next(&is->video_frame_queue);//删除上一帧已经播放的帧，然后将index+1，下面video_display播放时就是这个方法中的当前帧了

display:
	video_display(is);
}

int video_play_thread(void* arg) {
	play_stat_t* is = (play_stat_t*)arg;
	double remaining_time = 0.0;
	while (true)
	{
		if (is->quit)
			break;
		if (is->video_pkt_queue.nb_packets <= 0 && is->video_frame_queue.size <= 1) {
			cout << "video play to end" << endl;
			break;
		}
		if (remaining_time > 0.0) {
			av_usleep((unsigned)(remaining_time * 1000000.0));
		}
		remaining_time = REFRESH_RATE;
		video_refresh(is, &remaining_time);
	}
	return 0;
}

int queue_picture(play_stat_t* is, AVFrame* frame, double pts, double duration, int64_t pos)
{
	frame_t* vp;
	if (!(vp = queue::frame_queue_peek_writeable(&is->video_frame_queue))) {
		return -1;
	}
	vp->sar = frame->sample_aspect_ratio;
	vp->uploaded = 0;

	vp->width = frame->width;
	vp->height = frame->height;
	vp->format = frame->format;

	vp->pts = pts;
	vp->duration = duration;
	vp->pos = pos;

	av_frame_move_ref(vp->frame, frame);
	queue::frame_queue_push(&is->video_frame_queue);

	return 0;
}

int video_decode_frame(AVCodecContext* ctx, packet_queue_t* queue, AVFrame* frame) {
	int ret;
	while (true)
	{
		AVPacket* pkt = av_packet_alloc();
		while (true)
		{
			ret = avcodec_receive_frame(ctx, frame);
			if (ret < 0) {
				if (ret == AVERROR_EOF) {
					cout << "INFO:Video decoder has been fully flushed" << endl;
					avcodec_flush_buffers(ctx);
					return 0;
				}
				else if (ret == AVERROR(EAGAIN))
				{
					//cout << "INFO:Video decoder need more packet" << endl;
					break;
				}
				else {
					cout << "ERROR:Uknown error" << endl;
					continue;
				}
			}
			else {
				frame->pts = frame->best_effort_timestamp;
				return 1;
			}
		}

		if (queue::packet_queue_get(queue, pkt, 1) < 0) {
			return -1;
		}
		//seek
		if (pkt->data == NULL || strcmp((char*)pkt->data, "flush") == 0) {
			cout << "video decoder-->seek occured" << endl;
			avcodec_flush_buffers(ctx);
		}
		else {
			if (avcodec_send_packet(ctx, pkt) == AVERROR(EAGAIN)) {
				cout << "ERROR:Video send and receive both got EAGAIN" << endl;
			}
			av_packet_unref(pkt);
		}
	}
}

int video_decode_thread(void* arg) {
	play_stat_t* is = (play_stat_t*)arg;
	AVFrame* frame = av_frame_alloc();
	//TODO
	double pts, duration;
	int ret, got_picture;
	AVRational tb = is->p_video_stream->time_base;
	AVRational frame_rate = av_guess_frame_rate(is->p_ifmt_ctx, is->p_video_stream, NULL);

	while (true)
	{
		if (is->quit)
			break;


		got_picture = video_decode_frame(is->p_video_ctx, &is->video_pkt_queue, frame);
		if (got_picture < 0) {
			av_frame_free(&frame);
			return -1;
		}

		duration = (frame_rate.num && frame_rate.den ? av_q2d({ frame_rate.den,frame_rate.num }) : 0);
		pts = (frame->pts == AV_NOPTS_VALUE) ? NAN : frame->pts * av_q2d(tb);
		ret = queue_picture(is, frame, pts, duration, frame->pkt_pos);
		if (ret < 0) {
			av_frame_free(&frame);
			return -1;
		}
		av_frame_unref(frame);
	}
}

int video::open_video_stream(play_stat_t* is)
{
	int ret;
	AVCodec* video_decoder = NULL;
	AVCodecContext* ctx = NULL;
	video_decoder = avcodec_find_decoder(is->p_video_stream->codecpar->codec_id);
	if (!video_decoder) {
		cout << "ERROR:Find video decoder failed" << endl;
		return -1;
	}
	ctx = avcodec_alloc_context3(video_decoder);
	//TODO
	avcodec_parameters_to_context(ctx, is->p_video_stream->codecpar);
	ret = avcodec_open2(ctx, video_decoder, NULL);
	//TODO
	is->p_video_ctx = ctx;

	SDL_CreateThread(video_decode_thread, "video decode thread", is);

	return ret;
}

int video::open_video_playing(play_stat_t* is)
{
	int ret, buf_size;
	uint8_t* buffer = NULL;

	is->p_frame_yuv = av_frame_alloc();
	//TODO

	buf_size = av_image_get_buffer_size(AV_PIX_FMT_YUV420P,
		is->p_video_ctx->width, is->p_video_ctx->height, 1);
	buffer = (uint8_t*)av_malloc(buf_size);
	//TODO:check buffer

	ret = av_image_fill_arrays(is->p_frame_yuv->data,
		is->p_frame_yuv->linesize,
		buffer,
		AV_PIX_FMT_YUV420P,
		is->p_video_ctx->width,
		is->p_video_ctx->height, 1);
	if (ret < 0) {
		cout << "ERROR:Fill video array failed" << endl;
		return -1;
	}

	is->video_convert_ctx = sws_getContext(is->p_video_ctx->width,
		is->p_video_ctx->height,
		is->p_video_ctx->pix_fmt,
		is->p_video_ctx->width,
		is->p_video_ctx->height,
		AV_PIX_FMT_YUV420P,
		SWS_BICUBIC,
		NULL, NULL, NULL);
	if (!is->video_convert_ctx) {
		cout << "ERROR:Initialize sws_context failed" << endl;
		return -1;
	}

	is->sdl_video.rect.x = 0;
	is->sdl_video.rect.y = 0;
	is->sdl_video.rect.w = is->p_video_ctx->width;
	is->sdl_video.rect.h = is->p_video_ctx->height;

	is->sdl_video.window = SDL_CreateWindow("player",
		SDL_WINDOWPOS_CENTERED,
		SDL_WINDOWPOS_CENTERED,
		is->p_video_ctx->width,
		is->p_video_ctx->height, SDL_WINDOW_OPENGL);
	//TODO:CHECK WINDOW
	is->sdl_video.render = SDL_CreateRenderer(is->sdl_video.window, -1, 0);
	//TODO:CHECK RENDER
	is->sdl_video.texture = SDL_CreateTexture(is->sdl_video.render,
		SDL_PIXELFORMAT_IYUV,
		SDL_TEXTUREACCESS_STREAMING,
		is->p_video_ctx->width,
		is->p_video_ctx->height);
	//TODO:CHECK TEXTURE

	SDL_CreateThread(video_play_thread, "video play thread", is);

	return 0;
}

int video::open_video(play_stat_t* is)
{
	open_video_stream(is);
	open_video_playing(is);
	return 0;
}



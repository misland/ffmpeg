#include <stdio.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_audio.h>
#include <SDL2/SDL_log.h>

int main(int argc, char *argv[])
{
  // av_register_all();
  AVFormatContext *fmt_ctx;

  if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER))
  {
    SDL_Log("SDL_Init() failed: %s\n", SDL_GetError());
  }
  SDL_Log("SDL Init successfully\n");

  int i, count = SDL_GetNumAudioDevices(0);
  SDL_Log("audio devices count:%d\n", count);
  for (i = 0; i < count; i++)
  {
    SDL_Log("Audio device %d : %s \n", i, SDL_GetAudioDeviceName(i, 0));
  }

  return 0;
}

// gcc -o ./exe/test test.c -I. -L ./SDL2/lib -L ./ffmpeg/lib -lmingw32 -lSDL2main -lSDL2
// gcc -o player ffplayer_audio.c -I. -L ./SDL2/lib -L ./ffmpeg/lib -lmingw32 -lavcodec -lavformat -lavutil -lswscale -lswresample -lSDL2main -lSDL2

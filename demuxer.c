/*
  author: loki
  date: 2021.6.24
  description: 简单的解复用示例，来自：https://www.cnblogs.com/leisure_chn/p/10506642.html
  参考雷神的解决方案（https://blog.csdn.net/leixiaohua1020/article/details/39767055）
  解决了特定封装格式视频文件分离后H264码流无法正常播放问题
  祝雷神在天堂幸福！！！
*/

#include <libavformat/avformat.h>
#include <stdio.h>
#include <stdlib.h>

// 参考雷神解决方案，对于mp4/flv/mkv格式的视频，需要特殊处理，手动添加SPS和PPS
// 否则分离出来的h264码流无法正常播放
// ts封装格式的视频则无需作此特殊处理
#define USE_H264BSF 1

int main(int argc, char *argv[])
{
  const char *input_file = argv[1];
  const char *output_video = argv[2];
  const char *output_audio = argv[3];

  FILE *fp_video = fopen(output_video, "wb");
  FILE *fp_audio = fopen(output_audio, "wb");

  AVFormatContext *fmt_ctx = NULL;
  int ret = avformat_open_input(&fmt_ctx, input_file, NULL, NULL);
  ret = avformat_find_stream_info(fmt_ctx, NULL);

  int video_index = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
  int audio_index = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
  if (video_index < 0 || audio_index < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "couldn't find video or audio stream\n");
    return -1;
  }
  av_log(NULL, AV_LOG_INFO, "video index:%d,audio index:%d\n", video_index, audio_index);

#ifdef USE_H264BSF
  AVBitStreamFilterContext *bsfc = av_bitstream_filter_init("h264_mp4toannexb");
#endif

  AVPacket pkt;
  av_init_packet(&pkt);
  pkt.data = NULL;
  pkt.size = 0;

  while (av_read_frame(fmt_ctx, &pkt) >= 0)
  {
    if (pkt.stream_index == video_index)
    {
#ifdef USE_H264BSF
      av_bitstream_filter_filter(bsfc, fmt_ctx->streams[video_index]->codec, NULL, &pkt.data, &pkt.size, pkt.data, pkt.size, 0);
#endif
      fwrite(pkt.data, 1, pkt.size, fp_video);

      av_log(NULL, AV_LOG_INFO, "write video pkt:pkt.size=%d,pkt.pos=%d\n", pkt.size, pkt.pos);
    }
    else if (pkt.stream_index == audio_index)
    {
      ret = fwrite(pkt.data, 1, pkt.size, fp_audio);
      av_log(NULL, AV_LOG_INFO, "write audio pkt:pkt.size=%d,pkt.pos=%d\n", pkt.size, pkt.pos);
    }
    av_packet_unref(&pkt);
  }
  av_log(NULL, AV_LOG_INFO, "Demuxing completely\n");

  avformat_close_input(&fmt_ctx);
  fclose(fp_video);
  fclose(fp_audio);
  return 0;
}

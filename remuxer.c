/*
  author: Loki
  date: 20210-6-30
  description: 转封装测试代码，来自：https://www.cnblogs.com/leisure_chn/p/10506662.html
*/
#include "libavformat/avformat.h"
#include "libavutil/avutil.h"

int main(int argc, char *argv[])
{
  AVFormatContext *ifmt_ctx = NULL, *ofmt_ctx = NULL;
  AVPacket pkt;
  const char *input = argv[1], *output = argv[2];
  int ret, i;
  int stream_index = 0;
  // 二维数组，存放源文件每个数据流的索引
  int *stream_mapping = 0;
  int stream_mapping_size = 0;

  ret = avformat_open_input(&ifmt_ctx, input, NULL, NULL);
  ret = avformat_find_stream_info(ifmt_ctx, NULL);
  av_dump_format(ifmt_ctx, 0, input, 0);
  stream_mapping_size = ifmt_ctx->nb_streams;
  stream_mapping = av_mallocz_array(stream_mapping_size, sizeof(*stream_mapping));

  avformat_alloc_output_context2(&ofmt_ctx, NULL, NULL, output);

  for (i = 0; i < ifmt_ctx->nb_streams; i++)
  {
    AVStream *out_stream;
    AVStream *in_stream = ifmt_ctx->streams[i];
    AVCodecParameters *in_codecpar = in_stream->codecpar;
    if (in_codecpar->codec_type != AVMEDIA_TYPE_VIDEO &&
        in_codecpar->codec_type != AVMEDIA_TYPE_AUDIO &&
        in_codecpar->codec_type != AVMEDIA_TYPE_SUBTITLE)
    {
      stream_mapping[i] = -1;
      continue;
    }
    stream_mapping[i] = stream_index++;

    //创建新的数据流，将输入的流信息复制到新的流中
    out_stream = avformat_new_stream(ofmt_ctx, NULL);
    ret = avcodec_parameters_copy(out_stream->codecpar, in_codecpar);
    out_stream->codecpar->codec_tag = 0;
  }

  // 创建输出文件
  if (!(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
  {
    ret = avio_open(&ofmt_ctx->pb, output, AVIO_FLAG_WRITE);
    if (ret < 0)
    {
      goto end;
    }
  }

  ret = avformat_write_header(ofmt_ctx, NULL);
  while (1)
  {
    AVStream *in_stream, *out_stream;
    ret = av_read_frame(ifmt_ctx, &pkt);
    if (ret < 0)
    {
      break;
    }
    in_stream = ifmt_ctx->streams[pkt.stream_index];

    if (pkt.stream_index >= stream_mapping_size ||
        stream_mapping[pkt.stream_index] < 0)
    {
      // 说明流不符合预期，抛弃
      av_packet_unref(&pkt);
      continue;
    }

    // 当源文件中有被丢弃的数据流时，就需要使用这里的值传递，将输出文件流与源文件流对应起来，进而转换时基
    pkt.stream_index = stream_mapping[pkt.stream_index];
    out_stream = ofmt_ctx->streams[pkt.stream_index];

    av_packet_rescale_ts(&pkt, in_stream->time_base, out_stream->time_base);
    pkt.pos = -1;

    ret = av_interleaved_write_frame(ofmt_ctx, &pkt);
    if (ret < 0)
    {
      printf("muxing failed\n");
      break;
    }
    av_packet_unref(&pkt);
  }

  av_write_trailer(ofmt_ctx);

end:
  avformat_close_input(&ifmt_ctx);
  if (ofmt_ctx && !(ofmt_ctx->oformat->flags & AVFMT_NOFILE))
  {
    avio_closep(&ofmt_ctx->pb);
  }
  avformat_free_context(ofmt_ctx);
  av_free(stream_mapping);

  return 0;
}

/*
author:Loki Zhao
date:2021.3.31
description:来自ffmpeg官方demo，添加注释，便于理解ffmpeg中过滤器处理过程
 */

#define _XOPEN_SOURCE 600 /* for usleep */
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>

/*
  基本的过滤器使用流程是:
  解码后的画面--->buffer过滤器---->其他过滤器---->buffersink过滤器--->处理完的画面
  所有的过滤器形成了过滤器链
  一定要的两个过滤器是buffer过滤器和buffersink过滤器
  前者的作用是将解码后的画面加载到过滤器链中,后者的作用是将处理好的画面从过滤器链中读取出来
*/

const char *filter_descr = "scale=320:180";
/* other way:
   scale=78:24 [scl]; [scl] transpose=cclock // assumes "[in]" and "[out]" to be input output pads respectively
 */

static AVFormatContext *fmt_ctx;
static AVCodecContext *dec_ctx;
AVFilterContext *buffersink_ctx;
AVFilterContext *buffersrc_ctx;
AVFilterGraph *filter_graph;
static int video_stream_index = -1;
static int64_t last_pts = AV_NOPTS_VALUE;
FILE *out;
static bool log_printed = false;

static int open_input_file(const char *filename)
{
  int ret;
  AVCodec *dec;

  if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
    return ret;
  }

  if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
    return ret;
  }

  /* select the video stream */
  ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
    return ret;
  }
  video_stream_index = ret;

  /* create decoding context */
  dec_ctx = avcodec_alloc_context3(dec);
  if (!dec_ctx)
    return AVERROR(ENOMEM);
  avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);

  /* init the video decoder */
  if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
    return ret;
  }

  return 0;
}

static int init_filters(const char *filters_descr)
{
  char args[512];
  int ret = 0;
  // 根据注释理解如下：
  // 所有的过滤器都已在ffmpeg中定义过，名字是固定的，根据名字直接获取即可
  const AVFilter *buffersrc = avfilter_get_by_name("buffer");
  const AVFilter *buffersink = avfilter_get_by_name("buffersink");
  AVFilterInOut *outputs = avfilter_inout_alloc();
  AVFilterInOut *inputs = avfilter_inout_alloc();
  AVRational time_base = fmt_ctx->streams[video_stream_index]->time_base;
  enum AVPixelFormat pix_fmts[] = {AV_PIX_FMT_GRAY8, AV_PIX_FMT_NONE};

  // AVFilterGraph管理所有的过滤器图像
  filter_graph = avfilter_graph_alloc();
  if (!outputs || !inputs || !filter_graph)
  {
    ret = AVERROR(ENOMEM);
    goto end;
  }

  // 过滤器需要知道视频的基本属性
  snprintf(args, sizeof(args),
           "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
           dec_ctx->width, dec_ctx->height, dec_ctx->pix_fmt,
           time_base.num, time_base.den,
           dec_ctx->sample_aspect_ratio.num, dec_ctx->sample_aspect_ratio.den);

  // 起始过滤器
  ret = avfilter_graph_create_filter(&buffersrc_ctx, buffersrc, "in",
                                     args, NULL, filter_graph);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot create buffer source\n");
    goto end;
  }

  // 最后一个过滤器，之后会将该过滤器中的图像传递给编码器
  ret = avfilter_graph_create_filter(&buffersink_ctx, buffersink, "out",
                                     NULL, NULL, filter_graph);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot create buffer sink\n");
    goto end;
  }

  ret = av_opt_set_int_list(buffersink_ctx, "pix_fmts", pix_fmts,
                            AV_PIX_FMT_NONE, AV_OPT_SEARCH_CHILDREN);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot set output pixel format\n");
    goto end;
  }

  // 为什么outputs是给buffersrc_ctx？
  // 因为第一个过滤器链的元素，只有输出引脚，即将接收到的frame传递给下一个过滤器
  // 所以，outputs是给buffersrc_ctx的
  outputs->name = av_strdup("in");
  outputs->filter_ctx = buffersrc_ctx;
  outputs->pad_idx = 0;
  outputs->next = NULL;

  // 同上，因为buffersink_ctx是过滤器链的最后一个
  // 它只接收经过过滤器处理好的frame，所以，它只有输入引脚
  inputs->name = av_strdup("out");
  inputs->filter_ctx = buffersink_ctx;
  inputs->pad_idx = 0;
  inputs->next = NULL;

  if ((ret = avfilter_graph_parse_ptr(filter_graph, filters_descr,
                                      &inputs, &outputs, NULL)) < 0)
    goto end;

  if ((ret = avfilter_graph_config(filter_graph, NULL)) < 0)
    goto end;

end:
  avfilter_inout_free(&inputs);
  avfilter_inout_free(&outputs);

  return ret;
}

static void save_file_yuv(AVFrame *frame)
{
  if (!log_printed)
  {
    av_log(NULL, AV_LOG_INFO, "width:%d,height:%d\n", frame->width, frame->height);
    av_log(NULL, AV_LOG_INFO, "linesize[0]:%d,linesize[1]:%d,linesize[2]:%d\n",
           frame->linesize[0], frame->linesize[1], frame->linesize[2]);
    log_printed = true;
  }
  int line_size = frame->width * frame->height;
  // 一帧的yuv数据，分别要连续写，即先把y分量写完，再写u分量，然后是v分量
  fwrite(frame->data[0], 1, line_size, out);
  // fwrite(frame->data[1], 1, line_size / 4, out);
  // fwrite(frame->data[2], 1, line_size / 4, out);
}

int main(int argc, char **argv)
{
  int ret;
  AVPacket packet;
  AVFrame *frame;
  AVFrame *filt_frame;

  if (argc != 2)
  {
    fprintf(stderr, "Usage: %s file\n", argv[0]);
    exit(1);
  }

  out = fopen("out.yuv", "w");

  frame = av_frame_alloc();
  filt_frame = av_frame_alloc();
  if (!frame || !filt_frame)
  {
    perror("Could not allocate frame");
    exit(1);
  }

  if ((ret = open_input_file(argv[1])) < 0)
    goto end;
  if ((ret = init_filters(filter_descr)) < 0)
    goto end;

  while (1)
  {
    if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
      break;

    if (packet.stream_index == video_stream_index)
    {
      ret = avcodec_send_packet(dec_ctx, &packet);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
        break;
      }

      while (ret >= 0)
      {
        ret = avcodec_receive_frame(dec_ctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        {
          break;
        }
        else if (ret < 0)
        {
          av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
          goto end;
        }

        // save_file_yuv(frame);
        frame->pts = frame->best_effort_timestamp;

        // 将解码后的原始帧传递给过滤器链进行处理
        if (av_buffersrc_add_frame_flags(buffersrc_ctx, frame, AV_BUFFERSRC_FLAG_KEEP_REF) < 0)
        {
          av_log(NULL, AV_LOG_ERROR, "Error while feeding the filtergraph\n");
          break;
        }

        // 将经过过滤器链处理完毕的帧拉出来，播放或其它
        while (1)
        {
          ret = av_buffersink_get_frame(buffersink_ctx, filt_frame);
          if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
            break;
          if (ret < 0)
            goto end;

          // save_file_yuv(filt_frame);
          av_frame_unref(filt_frame);
        }
        av_frame_unref(frame);
      }
    }
    av_packet_unref(&packet);
  }
end:
  fclose(out);
  avfilter_graph_free(&filter_graph);
  avcodec_free_context(&dec_ctx);
  avformat_close_input(&fmt_ctx);
  av_frame_free(&frame);
  av_frame_free(&filt_frame);

  if (ret < 0 && ret != AVERROR_EOF)
  {
    fprintf(stderr, "Error occurred: %s\n", av_err2str(ret));
    exit(1);
  }

  exit(0);
}

/*
  使用SDL2播放wav格式音频文件
  来自：https://blog.csdn.net/weixin_33819479/article/details/85898947
  做了一些调整：
  1、原代码播放完毕会循环，停不下来；
  2、去除了CLang的一些宏；
*/

#include <SDL2\SDL.h>

static int done = 0;

struct
{
  SDL_AudioSpec spec;
  Uint8 *sound;    /* Pointer to wave data */
  Uint32 soundlen; /* Length of wave data */
  int soundpos;    /* Current play position */
} wave;

/*
  @Param:stream 是用于播放的音频数据，每次进入该回调，会将len指定长度的音频数据复制到该stream
  @Param:len 每次复制音频数据的长度
*/
void SDLCALL fillerup(void *unused, Uint8 *stream, int len)
{
  Uint8 *waveptr;
  int waveleft;

  /* Set up the pointers */
  waveptr = wave.sound + wave.soundpos;
  waveleft = wave.soundlen - wave.soundpos;
  SDL_Log("waveleft:%d,len:%d\n", waveleft, len);
  /* 
    Go! 
    这里有2种情况 
    1.如果waveleft>len,则直接拷贝数据播放 
    2.如果waveleft<=len,则先把剩下的数据拷贝到stream,同时拷贝开头剩下的len-waveleft数据到stream
  */
  while (waveleft <= len)
  {
    // 播放完升下的数据
    SDL_memcpy(stream, waveptr, waveleft);
    // 要把数据流指针向前移，否则SDL会认为依然有声音待播放
    stream += waveleft;
    // len -= waveleft;

    done = 1;

    // 播放完毕后将指针指向音频数据的开头（如果这样会循环播放，停不下来）
    // waveptr = wave.sound;
    // waveleft = wave.soundlen;
    // wave.soundpos = 0;
  }
  SDL_memcpy(stream, waveptr, len);
  wave.soundpos += len;
}

int main(int argc, char *argv[])
{
  int i;
  char filename[4096];

  /* Enable standard application logging */
  SDL_LogSetPriority(SDL_LOG_CATEGORY_APPLICATION, SDL_LOG_PRIORITY_INFO);

  /* Load the SDL library */
  if (SDL_Init(SDL_INIT_AUDIO) < 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't initialize SDL: %s\n", SDL_GetError());
    return (1);
  }

  if (argc > 1)
  {
    SDL_strlcpy(filename, argv[1], sizeof(filename));
  }
  else
  {
    SDL_strlcpy(filename, "C:\\shot\\video\\test\\player\\exe\\sample.wav", sizeof(filename));
  }
  /* 
    Load the wave file into memory 
    SDL_LoadWAV这个函数读取wav音频文件后会进行下面操作 
    1.填充wave.spec结构体 
    2.将音频数据填充到wave.sound 
    3.将音频数据长度填到wave.soundlen
  */
  if (SDL_LoadWAV(filename, &wave.spec, &wave.sound, &wave.soundlen) == NULL)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't load %s: %s\n", filename, SDL_GetError());
    SDL_Quit();
    return -1;
  }

  // 播放的时候需要数据时会回调到fillerup获取音频播放数据
  wave.spec.callback = fillerup;

  // Show the list of available drivers
  SDL_Log("Available audio drivers:");
  for (i = 0; i < SDL_GetNumAudioDrivers(); ++i)
  {
    SDL_Log("%i: %s", i, SDL_GetAudioDriver(i));
  }

  // Initialize fillerup() variables
  if (SDL_OpenAudio(&wave.spec, NULL) < 0)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "Couldn't open audio: %s\n", SDL_GetError());
    SDL_FreeWAV(wave.sound);
    SDL_Quit();
    return -2;
  }

  SDL_Log("Using audio driver: %s\n", SDL_GetCurrentAudioDriver());

  // 参数0：开始播放 1：暂停
  SDL_PauseAudio(0);

  while (!done && (SDL_GetAudioStatus() == SDL_AUDIO_PLAYING))
    // 让线程等待，让设备播放音频，单位是毫秒
    SDL_Delay(1000);

  // Clean up on signal
  SDL_CloseAudio();
  SDL_FreeWAV(wave.sound);
  SDL_Quit();
  return (0);
}
/*
  author: Loki
  date: 2021-7-14
  description: 编码测试代码，接收yuv格式数据，将其编码为H264码流文件
  issue: 目前只能生成.h264/.ts格式的文件(因为这两种格式没有文件头也能正常播放)，后面还要添加封装的逻辑
*/

#include <stdio.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/imgutils.h>

int encode(AVCodecContext *ctx, AVFrame *frame, AVPacket *pkt, FILE *fp)
{
  int ret;
  if (frame)
  {
    printf("Info:send frame %d" PRId64 "\n", frame->pts);
  }

  ret = avcodec_send_frame(ctx, frame);
  if (ret < 0)
  {
    printf("ERROR:sending frame failed,ret:%d\n", ret);
    return -1;
  }

  while (ret >= 0)
  {
    ret = avcodec_receive_packet(ctx, pkt);
    if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
    {
      return 0;
    }
    else if (ret < 0)
    {
      printf("Error:encoding failed,ret:%d\n", ret);
      return -1;
    }
    printf("Info:writing packet %3d" PRId64 "(size=%5d)\n", pkt->pts, pkt->size);
    fwrite(pkt->data, 1, pkt->size, fp);
    av_packet_unref(pkt);
  }
  return 0;
}

int main(int argc, char *argv[])
{
  char *input = argv[1];
  char *output = argv[2];
  int width = atoi(argv[3]);
  int height = atoi(argv[4]);
  int fps = atoi(argv[5]);

  int ret;
  AVFormatContext *out_fmt_ctx = NULL;
  AVStream *stream;
  AVCodecContext *ctx;
  AVCodec *codec;
  AVPacket *pkt;
  uint8_t *pic_buf;
  AVFrame *frame;
  int y_size;
  FILE *fp_input = fopen(input, "rb");
  FILE *fp_output = fopen(output, "wb");

  // 为输出格式初始化AVFormatContext
  ret = avformat_alloc_output_context2(&out_fmt_ctx, NULL, NULL, output);
  stream = avformat_new_stream(out_fmt_ctx, 0);

  // 如果要将yuv编码成.h264码流，需要使用libx264，则使用的ffmpeg必须是gpl版本，因为libx264是gpl协议
  // 如果使用的是lgpl版本的ffmpeg，因不包含libx264，运行时会抛出“[h264_amf @ 08e75900] DLL amfrt64.dll failed to open”错误
  codec = avcodec_find_encoder(out_fmt_ctx->oformat->video_codec);
  if (!codec)
  {
    printf("Error:can not find encoder\n");
    goto end;
  }

  ctx = avcodec_alloc_context3(codec);
  ctx->codec_type = AVMEDIA_TYPE_VIDEO;
  ctx->pix_fmt = AV_PIX_FMT_YUV420P;
  ctx->codec_id = out_fmt_ctx->oformat->video_codec;
  ctx->width = width;
  ctx->height = height;
  ctx->time_base.num = 1;
  ctx->time_base.den = fps;

  ret = avcodec_parameters_from_context(out_fmt_ctx->streams[0]->codecpar, ctx);

  ret = avcodec_open2(ctx, codec, NULL);
  if (ret < 0)
  {
    printf("Error:open encoder failed,ret:%d\n", ret);
    goto end;
  }

  // 设置stream类型为video，否则在下面写入文件头信息时会抛出段错误
  // out_fmt_ctx->streams[0]->codecpar->codec_type = AVMEDIA_TYPE_VIDEO;
  // out_fmt_ctx->streams[0]->codecpar->codec_id = AV_CODEC_ID_H264;
  // out_fmt_ctx->streams[0]->codecpar->format = AV_PIX_FMT_YUV420P;
  // 如果不设置分辨率，写头信息时会返回-22，同时控制台打印“dimensions not set”错误
  // 设置了分辨率后写头信息时则会抛出段错误
  // 初步估计，跟分辨率没有关系，是有哪些地方设置的不正确
  // out_fmt_ctx->streams[0]->codecpar->width = width;
  // out_fmt_ctx->streams[0]->codecpar->height = height;

  printf("Info:stream[0]:type:%d,width:%d,height:%d\n",
         out_fmt_ctx->streams[0]->codecpar->codec_type,
         out_fmt_ctx->streams[0]->codecpar->width,
         out_fmt_ctx->streams[0]->codecpar->height);
  av_dump_format(out_fmt_ctx, 0, output, 1);

  // 只有ts格式输出文件能正常写入，因为ts格式文件没有文件头和尾
  // mp4和avi格式抛出错误
  // flv格式虽可以正常写入数据，但是生成文件无法正常播放

  // 之所以出现上面问题，因为不能直接将h264码流直接封装成mp4等文件，这些是容器
  // 需要先编码，后封装，所以要先编码成.h264格式的码流文件，再使用muxer封装成相应的格式
  ret = avformat_write_header(out_fmt_ctx, NULL);

  frame = av_frame_alloc();
  int pic_size = av_image_get_buffer_size(ctx->pix_fmt, width, height, 1);
  printf("Info:pic_size:%d\n", pic_size);
  pic_buf = (uint8_t *)av_malloc(pic_size);
  av_image_fill_arrays(frame->data, frame->linesize, pic_buf, ctx->pix_fmt,
                       ctx->width, ctx->height, 1);

  pkt = av_packet_alloc();
  av_new_packet(pkt, pic_size);
  y_size = ctx->width * ctx->height;

  int i = 0;
  // 一次读取一帧数据，送到编码器处理
  while (fread(pic_buf, 1, y_size * 3 / 2, fp_input) == y_size * 3 / 2)
  {
    frame->data[0] = pic_buf;
    frame->data[1] = pic_buf + y_size;
    frame->data[2] = pic_buf + y_size + y_size / 4;
    frame->width = width;
    frame->height = height;
    frame->format = AV_PIX_FMT_YUV420P;
    frame->pts = i++ * (stream->time_base.den) / ((stream->time_base.num) * ctx->time_base.den);
    // i++;
    ret = encode(ctx, frame, pkt, fp_output);
    if (ret == -1)
    {
      break;
    }
  }

  printf("Info:frame count:%d\n", i);
  printf("stream->time_base.den=%d\n", stream->time_base.den);
  printf("stream->time_base.num=%d\n", stream->time_base.num);

  encode(ctx, NULL, pkt, fp_output);
  av_write_trailer(out_fmt_ctx);

end:
  avformat_free_context(out_fmt_ctx);
  // avio_close(out_fmt_ctx->pb);
  avcodec_close(ctx);
  av_free(pic_buf);
  av_free(frame);
  fclose(fp_input);
  fclose(fp_output);

  return 0;
}
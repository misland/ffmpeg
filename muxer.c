/*
  author: Loki
  date: 20210-6-30
  description: 复用测试代码，来自：https://www.cnblogs.com/leisure_chn/p/10506653.html
*/

#include <stdbool.h>
#include "libavformat/avformat.h"
#include "libavcodec/avcodec.h"

int main(int argc, char *argv[])
{
  const char *video_input = argv[1];
  const char *audio_input = argv[2];
  const char *output = argv[3];
  int ret = 0;

  av_log(NULL, AV_LOG_INFO, "-----------Parameters-----------\n");
  av_log(NULL, AV_LOG_INFO, "input video:%s\ninput audio:%s\noutput file:%s\n",
         video_input, audio_input, output);

  // 打开视频输入
  AVFormatContext *v_fmt_ctx = NULL;
  ret = avformat_open_input(&v_fmt_ctx, video_input, NULL, NULL);
  ret = avformat_find_stream_info(v_fmt_ctx, NULL);
  int video_index = av_find_best_stream(v_fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, NULL, 0);
  if (video_index < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Couldn't find video stream in video file:%s\n", video_input);
    return -1;
  }
  AVStream *video_stream = v_fmt_ctx->streams[video_index];
  av_log(NULL, AV_LOG_INFO, "-----------Video stream index:%d-----------\n", video_index);
  // av_dump_format(v_fmt_ctx, video_index, video_input, 0);

  // 打开音频输入
  AVFormatContext *a_fmt_ctx = NULL;
  ret = avformat_open_input(&a_fmt_ctx, audio_input, NULL, NULL);
  ret = avformat_find_stream_info(a_fmt_ctx, NULL);
  int audio_index = av_find_best_stream(a_fmt_ctx, AVMEDIA_TYPE_AUDIO, -1, -1, NULL, 0);
  if (audio_index < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Couldn't find audio stream in audio file:%s\n", audio_input);
    return -1;
  }
  AVStream *audio_stream = a_fmt_ctx->streams[audio_index];
  av_log(NULL, AV_LOG_INFO, "-----------Audio stream index:%d-----------\n", audio_index);
  // av_dump_format(a_fmt_ctx, audio_index, audio_index, 0);

  // 新建一个输出，添加两路stream，一路视频，一路音频
  AVFormatContext *out_fmt_ctx = NULL;
  ret = avformat_alloc_output_context2(&out_fmt_ctx, NULL, NULL, output);

  AVStream *out_v_stream = avformat_new_stream(out_fmt_ctx, NULL);
  ret = avcodec_parameters_copy(out_v_stream->codecpar, video_stream->codecpar);

  AVStream *out_a_stream = avformat_new_stream(out_fmt_ctx, NULL);
  ret = avcodec_parameters_copy(out_a_stream->codecpar, audio_stream->codecpar);

  if (!(out_fmt_ctx->oformat->flags & AVFMT_NOFILE))
  {
    av_log(NULL, AV_LOG_INFO, "-----------Open output file-----------\n");
    ret = avio_open(&out_fmt_ctx->pb, output, AVIO_FLAG_WRITE);
  }

  av_log(NULL, AV_LOG_INFO, "-----------Output file info-----------\n");
  av_dump_format(out_fmt_ctx, 0, output, 1);

  // 写入输入文件头
  // 根据提供的封装格式，会将时基信息写入到文件header中
  ret = avformat_write_header(out_fmt_ctx, NULL);

  AVPacket vpkt;
  av_init_packet(&vpkt);
  vpkt.data = NULL;
  vpkt.size = 0;

  AVPacket apkt;
  av_init_packet(&apkt);
  apkt.data = NULL;
  apkt.size = 0;

  AVPacket *p_pkt = NULL;
  int64_t vdts = 0;
  int64_t adts = 0;
  int flag = 0;
  bool video_finished = false;
  bool audio_finished = false;
  bool v_or_a = false;

  // 从两路输入依次取得packet，交织编入输出中
  av_log(NULL, AV_LOG_INFO, "V/A\tPTS\tDTS\tSIZE\n");
  while (1)
  {
    // 读视频帧
    if (vpkt.data == NULL && (!video_finished))
    {
      while (1)
      {
        ret = av_read_frame(v_fmt_ctx, &vpkt);
        if ((ret == AVERROR_EOF) || (avio_feof(v_fmt_ctx->pb)))
        {
          av_log(NULL, AV_LOG_INFO, "-----------Video finished-----------\n");
          video_finished = true;
          vdts = AV_NOPTS_VALUE;
          break;
        }
        else if (ret < 0)
        {
          av_log(NULL, AV_LOG_ERROR, "-----------Video read failed-----------\n");
          goto end;
        }

        // 判断读到的frame是视频帧
        // 这里传入的是纯视频文件，所以这个判断是不用的，但是如果源文件中包含了音频，这个判断就不可缺
        if (vpkt.stream_index == video_index)
        {
          // 如果输出文件的封装格式与输入文件封装格式不同，那么时基也会不同，则需要进行时基转换
          // 这里输入的是flv，输出为mp4，需要转换
          av_packet_rescale_ts(&vpkt, video_stream->time_base, out_v_stream->time_base);
          vpkt.pos = -1;
          vpkt.stream_index = 0;
          vdts = vpkt.dts;
          break;
        }
        av_packet_unref(&vpkt);
      }
    }

    // 读音频帧
    if (apkt.data == NULL && (!audio_finished))
    {
      while (1)
      {
        ret = av_read_frame(a_fmt_ctx, &apkt);
        if ((ret == AVERROR_EOF) || avio_feof(a_fmt_ctx->pb))
        {
          av_log(NULL, AV_LOG_ERROR, "-----------Audio finished-----------\n");
          audio_finished = true;
          adts = AV_NOPTS_VALUE;
          break;
        }
        else if (ret < 0)
        {
          av_log(NULL, AV_LOG_ERROR, "-----------Audio read error-----------\n");
          goto end;
        }
        if (apkt.stream_index == audio_index)
        {
          // ret = av_compare_ts(vdts, out_v_stream->time_base, adts, out_a_stream->time_base);
          av_packet_rescale_ts(&apkt, audio_stream->time_base, out_a_stream->time_base);

          apkt.pos = -1;
          apkt.stream_index = 1;
          adts = apkt.dts;
          break;
        }
        av_packet_unref(&apkt);
      }
    }

    if (video_finished && audio_finished)
    {
      av_log(NULL, AV_LOG_INFO, "-----------All read finished,flushing queue-----------\n");
      // av_interleaved_write_frame(out_fmt_ctx,NULL);
      break;
    }
    else
    {
      if (video_finished)
      {
        v_or_a = false;
      }
      else if (audio_finished)
      {
        v_or_a = true;
      }
      else
      {
        // 比较视频帧和音频帧的dts，哪个更小，将较小的帧先写入输出文件
        ret = av_compare_ts(vdts, video_stream->time_base, adts, audio_stream->time_base);
        v_or_a = (ret <= 0);
      }

      p_pkt = v_or_a ? &vpkt : &apkt;
      if (flag < 50)
      {
        av_log(NULL, AV_LOG_INFO, "%s\t%3" PRId64 "\t%3" PRId64 "\t%-5d\n", v_or_a ? "vp" : "ap", p_pkt->pts, p_pkt->dts, p_pkt->size);
      }
      ret = av_write_frame(out_fmt_ctx, p_pkt);
      flag++;
      if (p_pkt->data != NULL)
      {
        av_packet_unref(p_pkt);
      }
    }
  }

  av_write_trailer(out_fmt_ctx);
  av_log(NULL, AV_LOG_INFO, "-----------Muxing successed-----------\n");

end:
  avformat_close_input(&v_fmt_ctx);
  avformat_close_input(&a_fmt_ctx);
  avformat_free_context(out_fmt_ctx);
  return 0;
}

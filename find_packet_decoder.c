/*
  author:Loki Zhao
  description:解码封装过的视频，将每帧Y分量写到文件中
  date: 2021.3.31
  issues:
  1、Y分量文件用YUVPlayer播放时，刚开始是正常的，播放一会儿后异常
*/

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavfilter/buffersink.h>
#include <libavfilter/buffersrc.h>
#include <libavutil/opt.h>

static AVFormatContext *fmt_ctx;
static AVCodecContext *dec_ctx;
static int video_stream_index = -1;
static int64_t last_pts = AV_NOPTS_VALUE;
FILE *out;

static void save_file_y(unsigned char *buf, int wrap, int xsize, int ysize)
{
  for (int i = 0; i < ysize; i++)
  {
    fwrite(buf + i * wrap, 1, xsize, out);
  }
}

static int open_input_file(const char *filename)
{
  int ret;
  AVCodec *dec;

  if ((ret = avformat_open_input(&fmt_ctx, filename, NULL, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot open input file\n");
    return ret;
  }

  if ((ret = avformat_find_stream_info(fmt_ctx, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot find stream information\n");
    return ret;
  }

  /* select the video stream */
  ret = av_find_best_stream(fmt_ctx, AVMEDIA_TYPE_VIDEO, -1, -1, &dec, 0);
  if (ret < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot find a video stream in the input file\n");
    return ret;
  }
  video_stream_index = ret;

  /* create decoding context */
  dec_ctx = avcodec_alloc_context3(dec);
  if (!dec_ctx)
    return AVERROR(ENOMEM);
  avcodec_parameters_to_context(dec_ctx, fmt_ctx->streams[video_stream_index]->codecpar);

  /* init the video decoder */
  if ((ret = avcodec_open2(dec_ctx, dec, NULL)) < 0)
  {
    av_log(NULL, AV_LOG_ERROR, "Cannot open video decoder\n");
    return ret;
  }

  return 0;
}

int main(int argc, char **argv)
{
  int ret;
  AVPacket packet;
  AVFrame *frame;
  AVFrame *filt_frame;

  if (argc != 2)
  {
    av_log(NULL, AV_LOG_ERROR, "Please specify the file want to decode\n");
    exit(1);
  }

  out = fopen("out.yuv", "w");

  frame = av_frame_alloc();
  filt_frame = av_frame_alloc();
  if (!frame)
  {
    av_log(NULL, AV_LOG_ERROR, "Could not allocate frame");
    exit(1);
  }

  if ((ret = open_input_file(argv[1])) < 0)
    goto end;

  while (1)
  {
    if ((ret = av_read_frame(fmt_ctx, &packet)) < 0)
      break;

    if (packet.stream_index == video_stream_index)
    {
      ret = avcodec_send_packet(dec_ctx, &packet);
      if (ret < 0)
      {
        av_log(NULL, AV_LOG_ERROR, "Error while sending a packet to the decoder\n");
        break;
      }

      while (ret >= 0)
      {
        ret = avcodec_receive_frame(dec_ctx, frame);
        if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF)
        {
          break;
        }
        else if (ret < 0)
        {
          av_log(NULL, AV_LOG_ERROR, "Error while receiving a frame from the decoder\n");
          goto end;
        }

        save_file_y(frame->data[0], frame->linesize[0], dec_ctx->width, dec_ctx->height);
        av_frame_unref(frame);
      }
    }
    av_packet_unref(&packet);
  }
end:
  fclose(out);
  avcodec_free_context(&dec_ctx);
  avformat_close_input(&fmt_ctx);
  av_frame_free(&frame);

  if (ret < 0 && ret != AVERROR_EOF)
  {
    av_log(NULL, AV_LOG_ERROR, "Error occurred: %s\n", av_err2str(ret));
    exit(1);
  }

  exit(0);
}

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <sys/time.h>

#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_log.h>

#define SDL_AUDIO_BUFFER_SIZE 1024
#define MAX_AUDIO_FRAME_SIZE 19200

typedef struct packet_queue_t
{
  AVPacketList *first_pkt;
  AVPacketList *last_pkt;
  int nb_packets; // 队列中packet的总数
  int size;       // 队列中所有packet的大小（字节数）

  SDL_mutex *mutex; // SDL的互斥锁
  SDL_cond *cond;   // SDL的条件变量
} packet_queue_t;

typedef struct AudioParams
{
  int freq;
  int channels;
  int64_t channel_layout;
  enum AVSampleFormat fmt;
  int frame_size;
  int bytes_per_sec;
} FF_AudioParams;

static packet_queue_t s_audio_pkt_queue;

static FF_AudioParams s_audio_param_src;
static FF_AudioParams s_audio_param_tgt;

static struct SwrContext *s_audio_swr_ctx;
static uint8_t *s_resample_buf = NULL; // 需要重采样时的缓冲区
static int s_resample_buf_len = 0;     // 重采样时缓冲区的长度

static bool s_input_finished = false;  // 文件是否被读取完毕
static bool s_decode_finished = false; // 读取的packet是否被解码完毕
static bool s_print_log = false;       // 是否打印了日志

void packet_queue_init(packet_queue_t *q)
{
  memset(q, 0, sizeof(packet_queue_t));
  q->mutex = SDL_CreateMutex();
  q->cond = SDL_CreateCond();
}

int packet_queue_push(packet_queue_t *q, AVPacket *pkt)
{
  AVPacketList *pkt_list;
  if (av_packet_make_refcounted(pkt) < 0)
  {
    printf("ERROR: packet is not reference counted\n");
    return -1;
  }
  pkt_list = av_malloc(sizeof(AVPacketList));
  if (!pkt_list)
  {
    printf("ERROR: allocate memory for AVPacketList failed\n");
    return -1;
  }
  pkt_list->pkt = *pkt;
  pkt_list->next = NULL;   // 初始化单向链表的第一个元素
  SDL_LockMutex(q->mutex); // 为链表加锁，防止同时入栈
  if (!q->last_pkt)
  {
    // 此时说明队列是空的
    q->first_pkt = pkt_list;
  }
  else
  {
    // 队列非空，把新的packet追加到当前最后一个元素的后面
    q->last_pkt->next = pkt_list;
  }
  q->last_pkt = pkt_list;
  q->nb_packets++;
  q->size += pkt_list->pkt.size;

  SDL_CondSignal(q->cond); // 激活等待的一个线程

  SDL_UnlockMutex(q->mutex);
  return 0;
}

int packet_queue_pop(packet_queue_t *q, AVPacket *pkt, int block)
{
  AVPacketList *p_pkt_node;
  int ret;

  SDL_LockMutex(q->mutex);

  while ((1))
  {
    p_pkt_node = q->first_pkt;
    if (p_pkt_node)
    {
      // 队列非空，取出了packet，那么队列的第一个元素就要变成当前的第二个元素
      q->first_pkt = p_pkt_node->next;
      if (!q->first_pkt)
      {
        q->last_pkt = NULL;
      }
      q->nb_packets--;
      q->size -= p_pkt_node->pkt.size;
      *pkt = p_pkt_node->pkt;
      av_free(p_pkt_node);
      ret = 1;
      break;
    }
    else if (s_input_finished)
    {
      ret = 0;
      break;
    }
    else if (!block)
    {
      ret = 0;
      break;
    }
    else
    {
      // 队列为空且阻塞，等待信号
      SDL_CondWait(q->cond, q->mutex); // 必须在互斥锁锁上后才能调用，该方法会解锁互斥锁并等待拥有该锁的线程激活信号，激活后重新上锁
    }
  }

  SDL_UnlockMutex(q->mutex);
}

int audio_decode_frame(AVCodecContext *p_codec_ctx, AVPacket *p_packet, uint8_t *audio_buf, int buf_size)
{
  AVFrame *p_frame = av_frame_alloc();

  int frm_size = 0;
  int res = 0;
  int ret = 0;
  int nb_samples = 0; // 重采样输出样本数
  uint8_t *p_cp_buf = NULL;
  int cp_len = 0;
  bool need_new = false;

  while (1)
  {
    ret = avcodec_receive_frame(p_codec_ctx, p_frame);
    if (ret != 0)
    {
      if (ret == AVERROR_EOF)
      {
        printf("INFO:the decoder has been fully flushed\n ");
        goto exit;
      }
      else if (ret == AVERROR(EAGAIN))
      {
        // 缓冲区已经解码完，继续读取新的packet
        need_new = true;
      }
      else if (ret == AVERROR(EINVAL))
      {
        printf("ERROR: avcodec not opened,or it's an encoder\n");
        res = -1;
        goto exit;
      }
      else
      {
        printf("ERROR: error when decoding packet\n");
        res = -1;
        goto exit;
      }
    }

    else
    {
      if (!s_print_log)
      {
        printf("INFO:s_audio_param_src: channels:%d,channel_layout:%ld,sample_rate:%d,format:%d\n",
               s_audio_param_src.channels, s_audio_param_src.channel_layout, s_audio_param_src.freq, s_audio_param_src.fmt);
        printf("INFO:p_frame: channels:%d,channel_layout:%ld,sample_rate:%d,format:%d\n",
               p_frame->channels, p_frame->channel_layout, p_frame->sample_rate, p_frame->format);
        s_print_log = true;
      }
      if (p_frame->format != s_audio_param_src.fmt || p_frame->channel_layout != s_audio_param_src.channel_layout || p_frame->sample_rate != s_audio_param_src.freq)
      {
        swr_free(&s_audio_swr_ctx);
        s_audio_swr_ctx = swr_alloc_set_opts(NULL,
                                             s_audio_param_tgt.channel_layout, s_audio_param_tgt.fmt, s_audio_param_tgt.freq,
                                             p_frame->channel_layout, p_frame->format, p_frame->sample_rate,
                                             0, NULL);
        if (s_audio_swr_ctx == NULL || swr_init(s_audio_swr_ctx) < 0)
        {
          swr_free(&s_audio_swr_ctx);
          return -1;
        }

        s_audio_param_src.channel_layout = p_frame->channel_layout;
        s_audio_param_src.channels = p_frame->channels;
        s_audio_param_src.freq = p_frame->sample_rate;
        s_audio_param_src.fmt = p_frame->format;
      }

      if (s_audio_swr_ctx != NULL)
      {
        const uint8_t **in = (const uint8_t **)p_frame->extended_data;
        // 重采样输出音频缓冲区
        uint8_t **out = &s_resample_buf;
        int out_count = (int64_t)p_frame->nb_samples * s_audio_param_tgt.freq / p_frame->sample_rate + 256;
        int out_size = av_samples_get_buffer_size(NULL, s_audio_param_tgt.channels, out_count, s_audio_param_tgt.fmt, 0);
        if (out_size < 0)
        {
          printf("ERROR: av_samples_get_buffer_size failed \n");
          return 1;
        }

        if (s_resample_buf == NULL)
        {
          av_fast_malloc(&s_resample_buf, &s_resample_buf_len, out_size);
        }
        if (s_resample_buf == NULL)
        {
          return AVERROR(ENOMEM);
        }

        nb_samples = swr_convert(s_audio_swr_ctx, out, out_size, in, p_frame->nb_samples);
        if (nb_samples < 0)
        {
          printf("ERROR: swr_convert error\n");
          return -1;
        }
        if (nb_samples == out_count)
        {
          printf("audio buffer is probally too small\n");
          if (swr_init(s_audio_swr_ctx) < 0)
          {
            swr_free(&s_audio_swr_ctx);
          }
        }

        p_cp_buf = s_resample_buf;
        cp_len = nb_samples * s_audio_param_tgt.channels * av_get_bytes_per_sample(s_audio_param_tgt.fmt);
      }
      else
      {
        frm_size = av_samples_get_buffer_size(NULL, p_codec_ctx->channels, p_frame->nb_samples, p_codec_ctx->sample_fmt, 1);
        printf("INFO:frame size %d,buffer size %d\n", frm_size, buf_size);
        assert(frm_size <= buf_size);

        p_cp_buf = p_frame->data[0];
        cp_len = frm_size;
      }

      SDL_memcpy(audio_buf, p_cp_buf, cp_len);
      res = cp_len;
      goto exit;
    }

    if (need_new)
    {
      ret = avcodec_send_packet(p_codec_ctx, p_packet);
      if (ret != 0)
      {
        printf("ERROR: avcodec_send_packet failed,error no:%d\n", ret);
        av_packet_unref(p_packet);
        ret = -1;
        goto exit;
      }
    }
  }

exit:
  av_frame_unref(p_frame);
  return res;
}

void sdl_audio_callback(void *userdata, uint8_t *stream, int len)
{
  printf("INFO:sdl_audio_callback len:%d\n", len);
  AVCodecContext *p_codec_ctx = (AVCodecContext *)userdata;
  int copy_len;
  int get_size;
  int left = 0;

  static uint8_t s_audio_buf[(MAX_AUDIO_FRAME_SIZE * 3) / 2];
  static uint32_t s_audio_len = 0; // 获得的音频数据大小
  static uint32_t s_tx_idx = 0;    // 已发送给设备的数据量

  AVPacket *p_packet;

  int frm_size = 0;
  int ret_size = 0;
  int ret;

  while (len > 0)
  {
    // while begin
    if (s_decode_finished)
    {
      return;
    }

    if (s_tx_idx >= s_audio_len) // 之前解码的数据已经播放完了
    {
      p_packet = (AVPacket *)av_malloc(sizeof(AVPacket));
      // 先从packet队列中再拿一个packet来解码
      if (packet_queue_pop(&s_audio_pkt_queue, p_packet, 1) < 0)
      {
        if (s_input_finished)
        {
          av_packet_unref(p_packet);
          p_packet = NULL;
          printf("INFO:flushing decoder\n");
        }
        else
        {
          av_packet_unref(p_packet);
          return;
        }
      }

      // 然后解码拿到的packet
      get_size = audio_decode_frame(p_codec_ctx, p_packet, s_audio_buf, sizeof(s_audio_buf));
      SDL_Log("get_size:%d\n", get_size);
      if (get_size < 0)
      {
        // 获取音频packet失败
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "packet decode failed\n");
        s_audio_len = 1024;
        memset(s_audio_buf, 0, s_audio_len);
        av_packet_unref(p_packet);
      }
      else if (get_size == 0)
      {
        SDL_Log("decodeing finished\n");
        s_decode_finished = true;
      }
      else
      {
        s_audio_len = get_size;
        av_packet_unref(p_packet);
      }

      s_tx_idx = 0;
      if (p_packet->data != NULL)
      {
        // todo ?
      }
    }

    copy_len = s_audio_len - s_tx_idx;
    SDL_Log("copy_len:%d,len:%d", copy_len, len);

    if (copy_len > len)
    {
      left = copy_len - len;
      copy_len = len; // ?
    }
    else
    {
      left = 0;
    }
    SDL_Log("copy_len:%d,s_audio_len:%d,s_tx_idx:%d,left:%d\n", copy_len, s_audio_len, s_tx_idx, left);

    // 将解码后的音频（s_audio_buf）写入设备缓冲区（stream），设备播放
    // SDL_Log("copy data to stream\n");
    SDL_memcpy(stream, (uint8_t *)s_audio_buf + s_tx_idx, copy_len);
    len -= left > 0 ? left : copy_len;
    stream += copy_len;
    s_tx_idx += copy_len;
    SDL_Log("len:%d,s_tx_idx:%d,copy_len:%d", len, s_tx_idx, copy_len);

    // while end
  }
}

int main(int argc, char *argv[])
{
  AVFormatContext *p_fmt_ctx = NULL;
  AVCodecContext *p_codec_ctx = NULL;
  AVCodecParameters *p_codec_par = NULL;
  AVCodec *p_codec = NULL;
  AVPacket *p_packet = NULL;

  SDL_AudioSpec wanted_spec;
  SDL_AudioSpec actual_spec;

  int i = 0;
  int a_idx = -1;
  int ret = 0;
  int res = 0;

  if (argc < 2)
  {
    printf("ERROR: please specify the file you want to play\n");
    return -1;
  }

  ret = avformat_open_input(&p_fmt_ctx, argv[1], NULL, NULL);
  if (ret != 0)
  {
    printf("ERROR: avformat_open_input error,error no:%d\n", ret);
    res = -1;
    goto exit0;
  }

  ret = avformat_find_stream_info(p_fmt_ctx, NULL);
  if (ret < 0)
  {
    printf("ERROR: avformat_find_stream_info error,error no:%d\n", ret);
    res = -1;
    goto exit1;
  }

  a_idx = -1;
  for (i = 0; i < p_fmt_ctx->nb_streams; i++)
  {
    // printf("INFO:streams[%d]:codec_type:%d\n", i, p_fmt_ctx->streams[i]->codecpar->codec_type);
    if (p_fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
    {
      a_idx = i;
      printf("INFO: streams[%d] is audio stream\n", a_idx);
      break;
    }
  }
  if (a_idx == -1)
  {
    printf("ERROR: cannot find audio stream in the file\n");
    res = -1;
    goto exit1;
  }

  p_codec_par = p_fmt_ctx->streams[a_idx]->codecpar;
  p_codec = avcodec_find_decoder(p_codec_par->codec_id);
  // printf("INFO:codec_id:%d\n", p_codec_par->codec_id);
  if (p_codec_par == NULL)
  {
    printf("ERROR: cannot find decoder for audio stream\n");
    res = -1;
    goto exit1;
  }

  p_codec_ctx = avcodec_alloc_context3(p_codec);
  if (p_codec_ctx == NULL)
  {
    printf("ERROR: avcodec_alloc_context3 failed,error no:%d\n", ret);
    res = -1;
    goto exit1;
  }

  ret = avcodec_parameters_to_context(p_codec_ctx, p_codec_par);
  if (ret < 0)
  {
    printf("ERROR: avcodec_parameters_to_context failed,error no:%d\n", ret);
    res = -1;
    goto exit2;
  }

  ret = avcodec_open2(p_codec_ctx, p_codec, NULL);
  if (ret < 0)
  {
    printf("ERROR:avcodec_open2 failed,error no:%d\n", ret);
    res = -1;
    goto exit2;
  }

  p_packet = (AVPacket *)av_malloc(sizeof(AVPacket));
  if (p_packet == NULL)
  {
    printf("ERROR:av_malloc failed\n");
    res = -1;
    goto exit2;
  }

  if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_TIMER))
  {
    printf("ERROR: SDL_Init failed,error no:%s\n", SDL_GetError());
    res = -1;
    goto exit2;
  }

  packet_queue_init(&s_audio_pkt_queue);
  printf("INFO:p_codec_ctx:channels:%d,channel_layout:%ld,sample_rate:%d,format:%d\n",
         p_codec_ctx->channels, p_codec_ctx->channel_layout, p_codec_ctx->sample_rate, p_codec_ctx->sample_fmt);

  wanted_spec.freq = p_codec_ctx->sample_rate;
  wanted_spec.format = AUDIO_S16SYS; // 这里是设置一个默认的SDL解码器
  wanted_spec.channels = p_codec_ctx->channels;
  wanted_spec.silence = 0;
  wanted_spec.samples = SDL_AUDIO_BUFFER_SIZE;
  wanted_spec.callback = sdl_audio_callback;
  wanted_spec.userdata = p_codec_ctx;
  if (SDL_OpenAudio(&wanted_spec, &actual_spec) < 0)
  {
    printf("ERROR:SDL_OpenAudio failed,error no:%s\n", SDL_GetError());
    res = -1;
    goto exit4;
  }

  printf("INFO:actual_spec: channels:%d,sample_rate:%d,format:%d\n",
         actual_spec.channels, actual_spec.freq, actual_spec.format);
  s_audio_param_tgt.fmt = AV_SAMPLE_FMT_S16; // 这里是设置一个默认的ffmpeg解码器，因为并不知道要播放的音频编码格式，如果和默认的不一样，则解码时需要重采样
  // s_audio_param_tgt.fmt = p_codec_ctx->sample_fmt;
  s_audio_param_tgt.freq = actual_spec.freq;
  s_audio_param_tgt.channel_layout = av_get_default_channel_layout(actual_spec.channels);
  s_audio_param_tgt.channels = actual_spec.channels;
  s_audio_param_tgt.frame_size = av_samples_get_buffer_size(NULL, actual_spec.channels, 1, s_audio_param_tgt.fmt, 1);
  s_audio_param_tgt.bytes_per_sec = av_samples_get_buffer_size(NULL, actual_spec.channels, actual_spec.freq, s_audio_param_tgt.fmt, 1);
  printf("INFO:frame_size:%d,bytes_per_sec:%d\n", s_audio_param_tgt.frame_size, s_audio_param_tgt.bytes_per_sec);
  if (s_audio_param_tgt.bytes_per_sec <= 0 || s_audio_param_tgt.frame_size <= 0)
  {
    printf("ERROR: av_samples_get_get_buffer_size failed\n");
    res = -1;
    goto exit4;
  }
  s_audio_param_src = s_audio_param_tgt;
  printf("INFO:s_audio_param_src: channels:%d,channel_layout:%ld,sample_rate:%d,format:%d\n",
         s_audio_param_src.channels, s_audio_param_src.channel_layout, s_audio_param_src.freq, s_audio_param_src.fmt);

  SDL_PauseAudio(0);
  // 读取文件中的所有的音频packet，存放进链表中，供解码器解码后播放
  while (av_read_frame(p_fmt_ctx, p_packet) == 0)
  {
    if (p_packet->stream_index == a_idx)
    {
      packet_queue_push(&s_audio_pkt_queue, p_packet);
    }
    else
    {
      // printf("INFO:video stream,abandoned\n");
      av_packet_unref(p_packet);
    }
  }
  SDL_Delay(40);
  printf("INFO:file has been fully read\n");
  s_input_finished = true;

  while (!s_decode_finished)
  {
    SDL_Delay(1000);
  }
  printf("INFO:packets have been fully played\n");
  SDL_Delay(1000);

exit4:
  SDL_Quit();
exit3:
  av_packet_unref(p_packet);
exit2:
  avcodec_free_context(&p_codec_ctx);
exit1:
  avformat_close_input(&p_fmt_ctx);
exit0:
  if (s_resample_buf != NULL)
  {
    av_free(s_resample_buf);
  }
  return res;
}

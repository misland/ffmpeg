/*
  视频播放器 version 4，相比上一版，改动如下
  1、换掉过时的API:avcodec_decode_audio4，改用统一的send/receive API；

  2020.3.29-修改解码失败逻辑，不再直接返回，继续往下尝试解码
*/

#include <stdio.h>
#include <stdbool.h>
#include <libavcodec/avcodec.h>
#include <libavutil/imgutils.h>
#include <libavformat/avformat.h>
#include <libswscale/swscale.h>
#include <libswresample/swresample.h>
#include <libavutil/opt.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_video.h>
#include <SDL2/SDL_render.h>
#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_log.h>

#define SDL_USEREVENT_REFRESH (SDL_USEREVENT + 1)
#define SDL_AUDIO_BUFFER_SIZE 1024

static bool s_playing_exit = false;
static bool s_playing_pause = false;
static bool s_input_finished = false;  // 文件是否被读取完毕
static bool s_decode_finished = false; // 读取的packet是否被解码完毕

typedef struct packet_queue
{
  AVPacketList *first, *last;
  int nb_packets, size;
  SDL_mutex *mutex;
  SDL_cond *cond;
} packet_queue;

packet_queue audioq;
SwrContext *swr_ctx;
FILE *out;

void save_file_yuv(AVFrame *frame)
{
  av_log(NULL, AV_LOG_INFO, "width:%d,height:%d\n", frame->width, frame->height);
  // av_log(NULL, AV_LOG_INFO, "linesize[0]:%d,linesize[1]:%d,linesize[2]:%d\n",
  //        frame->linesize[0], frame->linesize[1], frame->linesize[2]);
  int line_size = frame->width * frame->height;
  // 一帧的yuv数据，分别要连续写，即先把y分量写完，再写u分量，然后是v分量
  fwrite(frame->data[0], 1, line_size, out);
  fwrite(frame->data[1], 1, line_size / 4, out);
  fwrite(frame->data[2], 1, line_size / 4, out);
}

void init_packet_queue(packet_queue *q)
{
  memset(q, 0, sizeof(packet_queue));
  q->mutex = SDL_CreateMutex();
  q->cond = SDL_CreateCond();
}

int packet_queue_put(packet_queue *q, const AVPacket *packet)
{
  AVPacketList *list;
  AVPacket pkt;
  int rv = 0;
  if (!q)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "packet_queue not initialized");
    rv = -1;
  }

  rv = av_packet_ref(&pkt, packet);

  list = (AVPacketList *)av_malloc(sizeof(AVPacketList));
  if (list == NULL)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "allocate memory for AVPacketList failed");
    rv = -1;
  }

  list->pkt = pkt;
  list->next = NULL;

  SDL_LockMutex(q->mutex);
  if (q->last == NULL)
  {
    q->first = list;
  }
  else
  {
    q->last->next = list;
  }
  q->last = list;
  q->nb_packets++;
  q->size += list->pkt.size;
  SDL_CondSignal(q->cond);

  SDL_UnlockMutex(q->mutex);
  // SDL_Log("packets:%d", q->nb_packets);
  return rv;
}

int packet_queue_pop(packet_queue *q, AVPacket *packet, int block)
{
  // SDL_Log("packet_queue_pop:come in");
  AVPacketList *pkt;
  int rv = 0;
  if (!q)
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "packet_queue not initialized");
    rv = -1;
    return rv;
  }

  SDL_LockMutex(q->mutex);
  while (1)
  {
    if (s_input_finished || s_playing_exit)
    {
      rv = -1;
      break;
    }

    pkt = q->first;
    if (pkt)
    {
      q->first = q->first->next;
      if (!q->first)
      {
        q->last = NULL;
      }
      q->nb_packets--;
      q->size -= pkt->pkt.size;

      *packet = pkt->pkt;
      // 最初想当然的写成了这样，结果坑了半天- -
      // 基础不牢固造成，对指针理解错误
      // packet = &pkt->pkt;
      av_free(pkt);
      rv = 1;
      break;
    }
    else if (!block)
    {
      break;
    }
    else
    {
      SDL_CondWait(q->cond, q->mutex);
    }
  }

  SDL_UnlockMutex(q->mutex);
  // SDL_Log("packet size:%d", packet->size);
  return rv;
}

int audio_frame_decode(AVCodecContext *codec_ctx, uint8_t *audio_buf, int buf_size)
{
  // SDL_Log("audio_frame_decode:come in");
  static AVPacket packet = {0};
  static uint8_t *packet_data = NULL;
  static int packet_size = 0;
  static AVFrame *frame = NULL;
  static uint8_t converted_data[(192000 * 3) / 2];
  static uint8_t *converted;
  bool need_new = false;

  int len1, len2, data_size = 0;
  if (!frame)
  {
    frame = av_frame_alloc();
    converted = &converted_data[0];
  }

  while (1)
  {
    while (packet_size > 0)
    {
      len1 = avcodec_receive_frame(codec_ctx, frame);
      if (len1 != 0)
      {
        if (len1 == AVERROR(EAGAIN))
        {
          // SDL_Log("need more audio packet");
          need_new = true;
          break;
        }
        else if (len1 == AVERROR_EOF)
        {
          s_decode_finished = true;
          SDL_Log("the decoder has been fully flushed");
          return -1;
        }
        else if (len1 == AVERROR(EINVAL))
        {
          SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "audio codec not opened");
          return -1;
        }
        else
        {
          SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "legitimate decoding error");
          return -1;
        }
      }
      else
      {
        packet_data += len1;
        packet_size -= len1;
        data_size = av_samples_get_buffer_size(NULL, codec_ctx->channels, frame->nb_samples, codec_ctx->sample_fmt, 1);
        int out_size = av_samples_get_buffer_size(NULL, codec_ctx->channels, frame->nb_samples, AV_SAMPLE_FMT_FLT, 1);
        len2 = swr_convert(swr_ctx, &converted, frame->nb_samples, (const uint8_t **)&frame->data[0], frame->nb_samples);
        memcpy(audio_buf, converted_data, out_size);
        data_size = out_size;
        if (data_size <= 0)
        {
          continue;
        }
        return data_size;
      }
    }

    if (packet.data)
    {
      av_free_packet(&packet);
    }

    if (s_decode_finished)
    {
      return -1;
    }

    if (packet_queue_pop(&audioq, &packet, 1) < 0)
    {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "get packet from queue error");
      return -1;
    }

    if (need_new)
    {
      len1 = avcodec_send_packet(codec_ctx, &packet);
      if (len1 != 0)
      {
        SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "send audio packet failed,error no:%d", len1);
        av_packet_unref(&packet);
        return -1;
      }
    }

    packet_data = packet.data;
    packet_size = packet.size;
  }

  return -1;
}

void sdl_audio_callback(void *userdata, uint8_t *stream, int len)
{
  // SDL_Log("sdl_audio_callback:come in,len:%d", len);
  AVCodecContext *codec_ctx = (AVCodecContext *)userdata;
  int len1, audio_size;
  static uint8_t audio_buf[(19200 * 3) / 2];
  static unsigned int audio_buf_size = 0;
  static unsigned int audio_buf_index = 0;

  while (len > 0)
  {
    if (audio_buf_index >= audio_buf_size)
    {
      audio_size = audio_frame_decode(codec_ctx, audio_buf, sizeof(audio_buf));
      // SDL_Log("audio_size:%d", audio_size);
      if (audio_size < 0)
      {
        // 返回负值，出错无法继续播放或已播放结束
        SDL_CloseAudio();
        break;
      }
      else
      {
        audio_buf_size = audio_size;
      }
      audio_buf_index = 0;
    }

    len1 = audio_buf_size - audio_buf_index;
    if (len1 > len)
    {
      len1 = len;
    }
    memcpy(stream, (uint8_t *)audio_buf + audio_buf_index, len1);
    len -= len1;
    stream += len1;
    audio_buf_index += len1;
  }
}

int sdl_thread_handle_refreshing(void *fps)
{
  SDL_Event sdl_event;
  int frame_rate = *((int *)fps);
  int interval = frame_rate > 0 ? 1000 / frame_rate : 40;
  // printf("frame rate %d fps,refreshing per %d ms\n", frame_rate, interval);
  while (!s_playing_exit)
  {
    if (!s_playing_pause)
    {
      sdl_event.type = SDL_USEREVENT_REFRESH;
      SDL_PushEvent(&sdl_event);
    }
    SDL_Delay(interval);
  }

  return 0;
}

int main(int argc, char *argv[])
{
  AVFormatContext *p_fmt_ctx = NULL;
  AVCodecContext *p_codec_ctx = NULL;
  AVCodecContext *audio_ctx = NULL;
  AVCodec *audio_codec = NULL;
  AVCodecParameters *p_codec_par = NULL;
  AVCodec *p_codec = NULL;
  AVFrame *p_frm_raw = NULL;
  AVFrame *p_frm_yuv = NULL;
  AVPacket *p_packet = NULL;
  SDL_AudioSpec wanted_spec = {0}, actual_spec = {0};
  struct SwsContext *sws_ctx = NULL;
  int buf_size;
  uint8_t *buffer = NULL;
  int i;
  int v_idx;
  int a_idx;
  int ret;
  int frame_rate;
  SDL_Window *screen;
  SDL_Renderer *sdl_renderer;
  SDL_Texture *sdl_texture;
  SDL_Rect sdl_rect;
  SDL_Thread *sdl_thread;
  SDL_Event sdl_event;

  if (argc < 2)
  {
    printf("please specify the video to play\n");
    return -1;
  }

  out = fopen("out.yuv", "w");
  if (out == NULL)
  {
    printf("ERROR:open file failed\n");
    return -1;
  }
  ret = avformat_open_input(&p_fmt_ctx, argv[1], NULL, NULL);
  if (ret != 0)
  {
    printf("avformat_open_input failed\n");
    return -1;
  }

  ret = avformat_find_stream_info(p_fmt_ctx, NULL);
  if (ret < 0)
  {
    printf("avformat_find_stream_info failed \n");
    return -1;
  }

  // av_dump_format(p_fmt_ctx, 0, argv[1], 0);

  v_idx = -1;
  for (i = 0; i < p_fmt_ctx->nb_streams; i++)
  {
    if (p_fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO)
    {
      v_idx = i;
      frame_rate = p_fmt_ctx->streams[i]->r_frame_rate.num / p_fmt_ctx->streams[i]->r_frame_rate.den;
      // printf("streams[%d] is video stream,fps:%d \n", v_idx, frame_rate);
    }
    else if (p_fmt_ctx->streams[i]->codecpar->codec_type == AVMEDIA_TYPE_AUDIO)
    {
      a_idx = i;
    }
  }

  if (v_idx == -1)
  {
    printf("cannot find video stream,please be sure the file you selected is a video file\n");
    return -1;
  }

  // 将视频解码器参数赋值给p_codec_par
  p_codec_par = p_fmt_ctx->streams[v_idx]->codecpar;
  // 有了解码器信息，可以在ffmpeg组件中寻找对应解码器了
  p_codec = avcodec_find_decoder(p_codec_par->codec_id);
  if (p_codec == NULL)
  {
    printf("cannot find proper decoder for this file format\n");
    return -1;
  }
  // 找到解码器后，用解码器初始化结构体p_codec_ctx
  p_codec_ctx = avcodec_alloc_context3(p_codec);
  // todo
  // 暂时还不理解，已经用p_codec_par中包含的p_codec初始化了p_codec_ctx，为什么还要继续这一步？
  ret = avcodec_parameters_to_context(p_codec_ctx, p_codec_par);
  if (ret < 0)
  {
    printf("avcodec_parameters_to_context failed,error no:%d\n", ret);
    return -1;
  }

  ret = avcodec_open2(p_codec_ctx, p_codec, NULL);
  if (ret < 0)
  {
    printf("avcodec_open2 failed,error no:%d\n", ret);
    return -1;
  }

  p_frm_raw = av_frame_alloc();
  p_frm_yuv = av_frame_alloc();

  // need to import : <libavutil/imgutils.h>
  // 根据视频的分辨率计算一帧yuv格式数据占用多少字节
  buf_size = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, p_codec_ctx->width, p_codec_ctx->height, 1);

  buffer = (uint8_t *)av_malloc(buf_size);
  // 为p_frm_yuv分配空间，因为没有方法为其分配空间，所以事先手动分配
  // p_frm_raw在av_read_frame时会分配，所以这里不用手动分配
  av_image_fill_arrays(p_frm_yuv->data, p_frm_yuv->linesize, buffer,
                       AV_PIX_FMT_YUV420P, p_codec_ctx->width, p_codec_ctx->height, 1);

  //初始化SwsContext，用于后面的sws_scale
  sws_ctx = sws_getContext(p_codec_ctx->width, p_codec_ctx->height, p_codec_ctx->pix_fmt, p_codec_ctx->width, p_codec_ctx->height,
                           AV_PIX_FMT_YUV420P, SWS_BICUBIC,
                           NULL, NULL, NULL);

  if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER))
  {
    SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL_Init failed,error no:%s\n", SDL_GetError());
    return -1;
  }

  // todo:异常处理
  audio_codec = avcodec_find_decoder(p_fmt_ctx->streams[a_idx]->codecpar->codec_id);
  audio_ctx = avcodec_alloc_context3(audio_codec);
  avcodec_parameters_to_context(audio_ctx, p_fmt_ctx->streams[a_idx]->codecpar);
  avcodec_open2(audio_ctx, audio_codec, NULL);
  swr_ctx = swr_alloc();
  av_opt_set_channel_layout(swr_ctx, "in_channel_layout", audio_ctx->channel_layout, 0);
  av_opt_set_channel_layout(swr_ctx, "out_channel_layout", audio_ctx->channel_layout, 0);
  av_opt_set_int(swr_ctx, "in_sample_rate", audio_ctx->sample_rate, 0);
  av_opt_set_int(swr_ctx, "out_sample_rate", audio_ctx->sample_rate, 0);
  av_opt_set_sample_fmt(swr_ctx, "in_sample_fmt", audio_ctx->sample_fmt, 0);
  av_opt_set_sample_fmt(swr_ctx, "out_sample_fmt", AV_SAMPLE_FMT_FLT, 0);
  swr_init(swr_ctx);
  if (audio_codec)
  {
    init_packet_queue(&audioq);
    wanted_spec.channels = audio_ctx->channels;
    wanted_spec.freq = audio_ctx->sample_rate;
    wanted_spec.format = AUDIO_F32;
    wanted_spec.silence = 0;
    wanted_spec.samples = SDL_AUDIO_BUFFER_SIZE;
    wanted_spec.userdata = audio_ctx;
    wanted_spec.callback = sdl_audio_callback;

    if (SDL_OpenAudio(&wanted_spec, &actual_spec) < 0)
    {
      SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "SDL_OpenAudio: %s\n", SDL_GetError());
      goto exit;
    }

    SDL_PauseAudio(0);
  }

  screen = SDL_CreateWindow("avsync player", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                            p_codec_ctx->width, p_codec_ctx->height,
                            SDL_WINDOW_OPENGL);
  if (screen == NULL)
  {
    printf("SDL_CreateWindow failed,error no:%s\n", SDL_GetError());
    return -1;
  }

  sdl_renderer = SDL_CreateRenderer(screen, -1, 0);

  //创建SDL_Texture
  //一个SDL_Texture对应一帧YUV数据
  //在ffmpeg中的AV_PIX_FMT_YUV420P，SDL中对应的是SDL_PIXELFORMAT_IYUV
  sdl_texture = SDL_CreateTexture(sdl_renderer,
                                  SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING,
                                  p_codec_ctx->width, p_codec_ctx->height);

  sdl_rect.x = 0;
  sdl_rect.y = 0;
  sdl_rect.w = p_codec_ctx->width;
  sdl_rect.h = p_codec_ctx->height;

  p_packet = (AVPacket *)av_malloc(sizeof(AVPacket));

  sdl_thread = SDL_CreateThread(sdl_thread_handle_refreshing, NULL, (void *)&frame_rate);
  if (sdl_thread == NULL)
  {
    printf("SDL_CreateThread failed,error no:%s\n", SDL_GetError());
    return -1;
  }

  while (1)
  {
    SDL_WaitEvent(&sdl_event);
    if (sdl_event.type == SDL_USEREVENT_REFRESH)
    {
      //开始从视频文件中读取packet
      //packet可能是视频、音频或者其他格式数据，但是解码器只能解视频和音频
      //非视频和音频格式数据并不会被丢弃，用于向解码器提供信息
      //视频：一个packet只包含一帧图像，即一个frame
      //音频：若帧长固定，一个packet可能包含整个frame，若帧长可变，一个packet只会包含一个frame
      while ((ret = av_read_frame(p_fmt_ctx, p_packet)) == 0)
      {
        if (p_packet->stream_index == a_idx)
        {
          // 音频frame压入队列，等待解码播放
          // SDL_Log("send audio packet,size:%d", p_packet->size);
          packet_queue_put(&audioq, p_packet);
        }
        else if (p_packet->stream_index == v_idx)
        {
          // 视频frame直接展示
          // 读到一个经过编码的frame，发送给解码器
          ret = avcodec_send_packet(p_codec_ctx, p_packet);
          if (ret != 0)
          {
            SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "avcodec_send_packet failed,error no:%d \n", ret);
            return -1;
          }

          //解码器接收编码后的帧，解码成原始帧
          ret = avcodec_receive_frame(p_codec_ctx, p_frm_raw);
          if (ret != 0)
          {
            if (ret == AVERROR_EOF)
            {
              SDL_Log("avcodec_receive_frame:video file has been played complely\n");
              s_input_finished = true;
              goto exit;
            }
            else
            {
              SDL_LogError(SDL_LOG_CATEGORY_APPLICATION, "avcodec_receive_frame failed,error no:%d\n", ret);
              // 对于有些视频，前几帧会返回-11，这时可以继续解码，后面能正常解出视频，直接返回也不太合适
              continue;
              // return -1;
            }
          }

          av_log(NULL, AV_LOG_INFO, "frame width:%d,frame height:%d\n", p_frm_raw->width, p_frm_raw->height);
          int line_data_size = p_codec_ctx->width * p_codec_ctx->height;
          fwrite(p_frm_raw->data[0], 1, line_data_size, out);
          fwrite(p_frm_yuv->data[1], 1, line_data_size / 4, out);
          fwrite(p_frm_yuv->data[2], 1, line_data_size / 4, out);
          // 将原始帧转换为yuv420格式
          sws_scale(sws_ctx,
                    (const uint8_t *const *)p_frm_raw->data, p_frm_raw->linesize,
                    0, p_codec_ctx->height,
                    p_frm_yuv->data, p_frm_yuv->linesize);
          SDL_UpdateYUVTexture(sdl_texture, &sdl_rect,
                               p_frm_yuv->data[0],     // y
                               p_frm_yuv->linesize[0], // y length
                               p_frm_yuv->data[1],     // u
                               p_frm_yuv->linesize[1],
                               p_frm_yuv->data[2], // v
                               p_frm_yuv->linesize[2]);
          // save_file_yuv(p_frm_yuv);
          // for (int i = 0; i < p_codec_ctx->height; i++)
          // {
          //   fwrite(p_frm_yuv->data[0] + i * p_frm_yuv->linesize[0], 1, p_codec_ctx->width, out);
          // }
          // int line_data_size = p_codec_ctx->width * p_codec_ctx->height;
          // fwrite(p_frm_yuv->data[0], 1, line_data_size, out);
          // fwrite(p_frm_yuv->data[1], 1, line_data_size / 4, out);
          // fwrite(p_frm_yuv->data[2], 1, line_data_size / 4, out);

          SDL_RenderClear(sdl_renderer);

          SDL_RenderCopy(sdl_renderer, sdl_texture, NULL, &sdl_rect);

          SDL_RenderPresent(sdl_renderer);
          break;
        }
      }

      av_packet_unref(p_packet);
      if (ret < 0)
      {
        SDL_Log("file has been played completely");
        s_input_finished = true;
        s_playing_exit = true;
        break;
      }
    }

    else if (sdl_event.type == SDL_KEYDOWN)
    {
      if (sdl_event.key.keysym.sym == SDLK_SPACE)
      {
        s_playing_pause = !s_playing_pause;
        printf("player %s \n", s_playing_pause ? "pause" : "playing");
      }
    }

    else if (sdl_event.type == SDL_QUIT)
    {
      printf("SDL event: QUIT \n");
      s_playing_exit = true;
      break;
    }

    else
    {
      SDL_Log("ignore SDL event :0x%04X\n", sdl_event.type);
    }
  }

exit:
  SDL_Quit();
  printf("INFO:SDL Quit \n");
  fclose(out);
  sws_freeContext(sws_ctx);
  av_free(buffer);
  av_frame_free(&p_frm_yuv);
  av_frame_free(&p_frm_raw);
  avcodec_close(p_codec_ctx);
  avformat_close_input(&p_fmt_ctx);

  return 0;
}
